# echo.ko

This is a reimplementation of the [FreeBSD echo character device tutorial](
https://www.freebsd.org/doc/en/books/arch-handbook/driverbasics-char.html) in
rust! I'm using this as a way to try out ideas for the olp driver that I'm
working on.

This is solely a toy project, and is not recommended for general consumption.
