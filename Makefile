RUST_SRCS!=find src -name '*.rs'
SRCS=echo.c
KMOD=echo
CLEANDIRS+=target/
TARGET=x86_64-kernel-freebsd
OUT_DIR=target/${TARGET}/debug
RUST_OBJ=${OUT_DIR}/libechoko.a
OBJS:=echo.o ${RUST_OBJ} ${OBJS}
.PATH: ${.CURDIR}/@/libkern
LIBCLANG_PATH?=/usr/local/llvm34/lib

${OUT_DIR}:
	mkdir -p ${OUT_DIR}

#${OUT_DIR}/libcore.rlib: ${OUT_DIR} ../rust/src/libcore/lib.rs
	#rustc --target ${TARGET} -Z no-landing-pads --out-dir ${OUT_DIR} ../rust/src/libcore/lib.rs

${RUST_OBJ}: ${RUST_SRCS} Cargo.toml ${OUT_DIR} #${OUT_DIR}/libcore.rlib
	LIBCLANG_PATH=${LIBCLANG_PATH} cargo build -v --target ${TARGET}
	strip -d ${RUST_OBJ} -o echo.ko.debug

.include <bsd.kmod.mk>
