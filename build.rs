/* Before we start building our kernel module, we need to generate
 * rust bindings from kernel header files.
 */

extern crate bindgen;

use std::path::Path;
use std::fs;
use std::io;
use std::io::{BufRead,Write};

fn main() {
    let gen_file = Path::new("src/gen.rs");
    let header = Path::new("gen_header.rs");
    let mut build = bindgen::builder();

    build.clang_arg("-I.")
         .header("headers.h")
         .emit_builtins();

    if ! fs::metadata(gen_file).is_ok() {
        match gen(gen_file, header, build) {
            Ok(()) => { println!("wrote bindings") },
            Err(e) => { println!("failed to write bindings: {}", e) }
        }
    }
}

fn gen(filename: &Path, header: &Path, build: bindgen::Builder) -> Result<(), io::Error> {
    let mut gen_out = Box::new(io::BufWriter::new(try!(fs::File::create(filename))));
    let header_reader = io::BufReader::new(try!(fs::File::open(header)));

    for line in header_reader.lines() {
        try!(gen_out.write(line.unwrap().as_bytes()));
        try!(gen_out.write("\n".as_bytes()));
    }

    match build.generate() {
        Ok(bindings) => bindings.write(gen_out),
        Err(()) => Err(io::Error::new(io::ErrorKind::Other, "Could not generate bidings"))
    }
}
