/*
 * Copyright (c) 2015 William Orr <will@worrbase.com>. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * Redistributions of source code must retain the above copyright notice, this list
 * of conditions and the following disclaimer.  Redistributions in binary form must
 * reproduce the above copyright notice, this list of conditions and the following
 * disclaimer in the documentation and/or other materials provided with the
 * distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 * ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
#![feature(const_fn)]
#![feature(lang_items)]
#![feature(no_std)]
#![feature(unique)]
#![feature(core_str_ext)]
#![no_std]

extern crate rlibc;
#[macro_use]
extern crate rcstring;

mod gen;

use core::mem;
use core::ptr::{self,Unique};
use gen::libc;

pub type Module = gen::Struct_module;
pub type CDev = gen::Struct_cdev;
pub type Thread = gen::Struct_thread;
pub type Uio = gen::Struct_uio;
pub type CDevSw = gen::Struct_cdevsw;
pub type Ucred = gen::Struct_ucred;

const ENOTSUPP: libc::c_int = 45;
const UID_ROOT: libc::c_uint = 0;
const GID_WHEEL: libc::c_uint = 0;
const MAKEDEV_CHECKNAME: libc::c_int = 0x20;
const MAKEDEV_WAITOK: libc::c_int = 0x08;
const D_VERSION: libc::c_int = 0x17122009;

#[no_mangle]
pub extern "C" fn echo_loader(modu: *mut Module, what: libc::c_int, arg: libc::c_void) -> libc::c_int {
    unsafe {
        gen::printf(cstr!("Entering echo_loader\n").into_raw());
        gen::printf(cstr!("modu: %p, what: %i, arg: %p\n").into_raw(), modu, what, arg);
    }
    let mut echo_cdevsw = unsafe {
        let mut echo_cdevsw = Unique::new(&mut CDevSw::default());

        echo_cdevsw.get_mut().d_open = &mut Some(echo_open);
        echo_cdevsw.get_mut().d_close = &mut Some(echo_close);
        echo_cdevsw.get_mut().d_read = &mut Some(echo_read);
        echo_cdevsw.get_mut().d_write = &mut Some(echo_write);
        echo_cdevsw.get_mut().d_version = D_VERSION;

        echo_cdevsw
    };

    static mut echo_dev: *mut CDev = ptr::null_mut();
    match what as libc::c_uint {
        gen::MOD_LOAD => {
            unsafe {
                gen::printf(cstr!("Loading echo device\n").into_raw());
                gen::make_dev_p(MAKEDEV_CHECKNAME | MAKEDEV_WAITOK,
                           &mut echo_dev,
                           echo_cdevsw.get_mut(),
                           mem::transmute(0u64),
                           UID_ROOT,
                           GID_WHEEL,
                           0o600,
                           cstr!("echo").into_raw())
            }
        },
        gen::MOD_UNLOAD => {
            unsafe {
                gen::printf(cstr!("Unloading device\n").into_raw());
                if ! echo_dev.is_null() {
                    gen::destroy_dev(echo_dev);
                }
            }

            0
        },
        _ => {
            ENOTSUPP
        }
    }
}

#[no_mangle]
pub extern "C" fn echo_open(dev: CDev, oflags: libc::c_int, devtype: libc::c_int, td: Thread) -> libc::c_int {
    unsafe { gen::printf(cstr!("Calling open\n").into_raw()) };
    0
}

#[no_mangle]
pub extern "C" fn echo_close(dev: CDev, fflag: libc::c_int, devtype: libc::c_int, td: Thread) -> libc::c_int {
    unsafe { gen::printf(cstr!("Calling close\n").into_raw()) };
    0
}

#[no_mangle]
pub extern "C" fn echo_read(dev: CDev, uio: Uio, ioflag: libc::c_int) -> libc::c_int {
    0
}

#[no_mangle]
pub extern "C" fn echo_write(dev: CDev, uio: Uio, ioflag: libc::c_int) -> libc::c_int {
    0
}

#[lang = "stack_exhausted"] extern fn stack_exhausted() {}
#[lang = "eh_personality"] extern fn eh_personality() {}
#[lang = "panic_fmt"] fn panic_fmt() -> ! { loop {} }
