#![allow(non_camel_case_types, non_snake_case,dead_code)]

pub mod libc {
	#[repr(u8)]
	pub enum c_void {
		// Two dummy variants so the #[repr] attribute can be used.
		#[doc(hidden)]
		__variant1,
		#[doc(hidden)]
		__variant2,
	}
	pub type int8_t = i8;
	pub type int16_t = i16;
	pub type int32_t = i32;
	pub type int64_t = i64;
	pub type uint8_t = u8;
	pub type uint16_t = u16;
	pub type uint32_t = u32;
	pub type uint64_t = u64;

	pub type c_schar = i8;
	pub type c_uchar = u8;
	pub type c_short = i16;
	pub type c_ushort = u16;
	pub type c_int = i32;
	pub type c_uint = u32;
	pub type c_float = f32;
	pub type c_double = f64;
	pub type c_longlong = i64;
	pub type c_ulonglong = u64;
	pub type intmax_t = i64;
	pub type uintmax_t = u64;

	pub type size_t = usize;
	pub type ptrdiff_t = isize;
	pub type intptr_t = isize;
	pub type uintptr_t = usize;
	pub type ssize_t = isize;

	pub enum FILE {}
	pub enum fpos_t {} // TODO: fill this out with a struct

    pub type c_char = i8;
    pub type c_long = i64;
    pub type c_ulong = u64;
    pub type time_t = i64;
    pub type suseconds_t = i64;


	pub type clock_t = i32;
	pub type dev_t = u32;
	pub type ino_t = u32;
	pub type mode_t = u16;
	pub type nlink_t = u16;
	pub type blksize_t = u32;
	pub type fflags_t = u32;
	pub type pthread_attr_t = *mut c_void;
	pub type rlim_t = i64;
	pub type pthread_mutex_t = *mut c_void;
	pub type pthread_mutexattr_t = *mut c_void;
	pub type pthread_cond_t = *mut c_void;
	pub type pthread_rwlock_t = *mut c_void;
	pub type pthread_key_t = c_int;

	pub enum timezone {}

pub type pid_t = i32;
pub type uid_t = u32;
pub type gid_t = u32;
pub type in_addr_t = u32;
pub type in_port_t = u16;
pub type sighandler_t = size_t;

pub enum DIR {}

    pub struct utimbuf {
        pub actime: time_t,
        pub modtime: time_t,
    }

    pub struct timeval {
        pub tv_sec: time_t,
        pub tv_usec: suseconds_t,
    }

    pub struct timespec {
        pub tv_sec: time_t,
        pub tv_nsec: c_long,
    }

    pub struct rlimit {
        pub rlim_cur: rlim_t,
        pub rlim_max: rlim_t,
    }

    pub struct rusage {
        pub ru_utime: timeval,
        pub ru_stime: timeval,
        pub ru_maxrss: c_long,
        pub ru_ixrss: c_long,
        pub ru_idrss: c_long,
        pub ru_isrss: c_long,
        pub ru_minflt: c_long,
        pub ru_majflt: c_long,
        pub ru_nswap: c_long,
        pub ru_inblock: c_long,
        pub ru_oublock: c_long,
        pub ru_msgsnd: c_long,
        pub ru_msgrcv: c_long,
        pub ru_nsignals: c_long,
        pub ru_nvcsw: c_long,
        pub ru_nivcsw: c_long,
    }

    pub struct in_addr {
        pub s_addr: in_addr_t,
    }

    pub struct in6_addr {
        pub s6_addr: [u8; 16],
        __align: [u32; 0],
    }

    pub struct ip_mreq {
        pub imr_multiaddr: in_addr,
        pub imr_interface: in_addr,
    }

    pub struct ipv6_mreq {
        pub ipv6mr_multiaddr: in6_addr,
        pub ipv6mr_interface: c_uint,
    }

    pub struct Dl_info {
        pub dli_fname: *const c_char,
        pub dli_fbase: *mut c_void,
        pub dli_sname: *const c_char,
        pub dli_saddr: *mut c_void,
    }

	pub const WNOHANG: c_int = 1;
	pub const SIG_DFL: sighandler_t = 0 as sighandler_t;
	pub const SIG_IGN: sighandler_t = 1 as sighandler_t;
	pub const SIG_ERR: sighandler_t = !0 as sighandler_t;

	pub type wchar_t = i32;
	pub type off_t = i64;
	pub type useconds_t = u32;
	pub type blkcnt_t = i64;
	pub type socklen_t = u32;
	pub type sa_family_t = u8;
	pub type pthread_t = uintptr_t;
	pub type fsblkcnt_t = c_uint;
	pub type fsfilcnt_t = c_uint;

	pub struct sockaddr {
		pub sa_len: u8,
		pub sa_family: sa_family_t,
		pub sa_data: [c_char; 14],
	}

	pub struct sockaddr_in {
		pub sin_len: u8,
		pub sin_family: sa_family_t,
		pub sin_port: in_port_t,
		pub sin_addr: in_addr,
		pub sin_zero: [c_char; 8],
	}

	pub struct sockaddr_in6 {
		pub sin6_len: u8,
		pub sin6_family: sa_family_t,
		pub sin6_port: in_port_t,
		pub sin6_flowinfo: u32,
		pub sin6_addr: in6_addr,
		pub sin6_scope_id: u32,
	}

	pub struct sockaddr_un {
		pub sun_len: u8,
		pub sun_family: sa_family_t,
		pub sun_path: [c_char; 104]
	}

	pub struct passwd {
		pub pw_name: *mut c_char,
		pub pw_passwd: *mut c_char,
		pub pw_uid: uid_t,
		pub pw_gid: gid_t,
		pub pw_change: time_t,
		pub pw_class: *mut c_char,
		pub pw_gecos: *mut c_char,
		pub pw_dir: *mut c_char,
		pub pw_shell: *mut c_char,
		pub pw_expire: time_t,

		#[cfg(not(any(target_os = "macos", target_os = "ios")))]
		pub pw_fields: c_int,
	}

	pub struct ifaddrs {
		pub ifa_next: *mut ifaddrs,
		pub ifa_name: *mut c_char,
		pub ifa_flags: c_uint,
		pub ifa_addr: *mut sockaddr,
		pub ifa_netmask: *mut sockaddr,
		pub ifa_dstaddr: *mut sockaddr,
		pub ifa_data: *mut c_void
	}

	pub struct fd_set {
		fds_bits: [i32; FD_SETSIZE / 32],
	}

	pub struct tm {
		pub tm_sec: c_int,
		pub tm_min: c_int,
		pub tm_hour: c_int,
		pub tm_mday: c_int,
		pub tm_mon: c_int,
		pub tm_year: c_int,
		pub tm_wday: c_int,
		pub tm_yday: c_int,
		pub tm_isdst: c_int,
		pub tm_gmtoff: c_long,
		pub tm_zone: *mut c_char,
	}

	pub const FIOCLEX: c_ulong = 0x20006601;
	pub const FIONBIO: c_ulong = 0x8004667e;

	pub const SA_ONSTACK: c_int = 0x0001;
	pub const SA_SIGINFO: c_int = 0x0040;
	pub const SA_RESTART: c_int = 0x0002;
	pub const SA_RESETHAND: c_int = 0x0004;
	pub const SA_NOCLDSTOP: c_int = 0x0008;
	pub const SA_NODEFER: c_int = 0x0010;
	pub const SA_NOCLDWAIT: c_int = 0x0020;

	pub const SIGCHLD: c_int = 20;
	pub const SIGBUS: c_int = 10;
	pub const SIG_SETMASK: c_int = 3;

	pub const IPV6_MULTICAST_LOOP: c_int = 11;
	pub const IPV6_V6ONLY: c_int = 27;

	pub const FD_SETSIZE: usize = 1024;

	pub const ST_RDONLY: c_ulong = 1;
	pub const ST_NOSUID: c_ulong = 2;

	pub const NI_MAXHOST: socklen_t = 1025;

    pub struct dirent {
        pub d_fileno: u32,
        pub d_reclen: u16,
        pub d_type: u8,
        pub d_namelen: u8,
        pub d_name: [c_char; 256],
    }

    pub struct glob_t {
        pub gl_pathc: size_t,
        __unused1: size_t,
        pub gl_offs: size_t,
        __unused2: c_int,
        pub gl_pathv:  *mut *mut c_char,

        __unused3: *mut c_void,

        __unused4: *mut c_void,
        __unused5: *mut c_void,
        __unused6: *mut c_void,
        __unused7: *mut c_void,
        __unused8: *mut c_void,
    }

    pub struct sockaddr_storage {
        pub ss_len: u8,
        pub ss_family: sa_family_t,
        __ss_pad1: [u8; 6],
        __ss_align: i64,
        __ss_pad2: [u8; 112],
    }

    pub struct addrinfo {
        pub ai_flags: c_int,
        pub ai_family: c_int,
        pub ai_socktype: c_int,
        pub ai_protocol: c_int,
        pub ai_addrlen: socklen_t,
        pub ai_canonname: *mut c_char,
        pub ai_addr: *mut sockaddr,
        pub ai_next: *mut addrinfo,
    }

    pub struct sigset_t {
        bits: [u32; 4],
    }

    pub struct siginfo_t {
        pub si_signo: c_int,
        pub si_errno: c_int,
        pub si_code: c_int,
        pub si_pid: pid_t,
        pub si_uid: uid_t,
        pub si_status: c_int,
        pub si_addr: *mut c_void,
        _pad: [c_int; 12],
    }

    pub struct sigaction {
        pub sa_sigaction: sighandler_t,
        pub sa_flags: c_int,
        pub sa_mask: sigset_t,
    }

    pub struct stack_t {
        pub ss_sp: *mut c_void,
        pub ss_size: size_t,
        pub ss_flags: c_int,
    }

    pub struct statvfs {
        pub f_bavail: fsblkcnt_t,
        pub f_bfree: fsblkcnt_t,
        pub f_blocks: fsblkcnt_t,
        pub f_favail: fsfilcnt_t,
        pub f_ffree: fsfilcnt_t,
        pub f_files: fsfilcnt_t,
        pub f_bsize: c_ulong,
        pub f_flag: c_ulong,
        pub f_frsize: c_ulong,
        pub f_fsid: c_ulong,
        pub f_namemax: c_ulong,
    }

	pub const EXIT_FAILURE: c_int = 1;
	pub const EXIT_SUCCESS: c_int = 0;
	pub const RAND_MAX: c_int = 0x7fff_fffd;
	pub const EOF: c_int = -1;
	pub const SEEK_SET: c_int = 0;
	pub const SEEK_CUR: c_int = 1;
	pub const SEEK_END: c_int = 2;
	pub const _IOFBF: c_int = 0;
	pub const _IONBF: c_int = 2;
	pub const _IOLBF: c_int = 1;
	pub const BUFSIZ: c_uint = 1024;
	pub const FOPEN_MAX: c_uint = 20;
	pub const FILENAME_MAX: c_uint = 1024;
	pub const L_tmpnam: c_uint = 1024;
	pub const TMP_MAX: c_uint = 308915776;

	pub const O_RDONLY: c_int = 0;
	pub const O_WRONLY: c_int = 1;
	pub const O_RDWR: c_int = 2;
	pub const O_APPEND: c_int = 8;
	pub const O_CREAT: c_int = 512;
	pub const O_EXCL: c_int = 2048;
	pub const O_NOCTTY: c_int = 32768;
	pub const O_TRUNC: c_int = 1024;
	pub const O_CLOEXEC: c_int = 0x00100000;
	pub const S_IFIFO: mode_t = 4096;
	pub const S_IFCHR: mode_t = 8192;
	pub const S_IFBLK: mode_t = 24576;
	pub const S_IFDIR: mode_t = 16384;
	pub const S_IFREG: mode_t = 32768;
	pub const S_IFLNK: mode_t = 40960;
	pub const S_IFSOCK: mode_t = 49152;
	pub const S_IFMT: mode_t = 61440;
	pub const S_IEXEC: mode_t = 64;
	pub const S_IWRITE: mode_t = 128;
	pub const S_IREAD: mode_t = 256;
	pub const S_IRWXU: mode_t = 448;
	pub const S_IXUSR: mode_t = 64;
	pub const S_IWUSR: mode_t = 128;
	pub const S_IRUSR: mode_t = 256;
	pub const S_IRWXG: mode_t = 56;
	pub const S_IXGRP: mode_t = 8;
	pub const S_IWGRP: mode_t = 16;
	pub const S_IRGRP: mode_t = 32;
	pub const S_IRWXO: mode_t = 7;
	pub const S_IXOTH: mode_t = 1;
	pub const S_IWOTH: mode_t = 2;
	pub const S_IROTH: mode_t = 4;
	pub const F_OK: c_int = 0;
	pub const R_OK: c_int = 4;
	pub const W_OK: c_int = 2;
	pub const X_OK: c_int = 1;
	pub const STDIN_FILENO: c_int = 0;
	pub const STDOUT_FILENO: c_int = 1;
	pub const STDERR_FILENO: c_int = 2;
	pub const F_LOCK: c_int = 1;
	pub const F_TEST: c_int = 3;
	pub const F_TLOCK: c_int = 2;
	pub const F_ULOCK: c_int = 0;
	pub const F_DUPFD_CLOEXEC: c_int = 17;
	pub const SIGHUP: c_int = 1;
	pub const SIGINT: c_int = 2;
	pub const SIGQUIT: c_int = 3;
	pub const SIGILL: c_int = 4;
	pub const SIGABRT: c_int = 6;
	pub const SIGFPE: c_int = 8;
	pub const SIGKILL: c_int = 9;
	pub const SIGSEGV: c_int = 11;
	pub const SIGPIPE: c_int = 13;
	pub const SIGALRM: c_int = 14;
	pub const SIGTERM: c_int = 15;

	pub const PROT_NONE: c_int = 0;
	pub const PROT_READ: c_int = 1;
	pub const PROT_WRITE: c_int = 2;
	pub const PROT_EXEC: c_int = 4;

	pub const MAP_FILE: c_int = 0x0000;
	pub const MAP_SHARED: c_int = 0x0001;
	pub const MAP_PRIVATE: c_int = 0x0002;
	pub const MAP_FIXED: c_int = 0x0010;
	pub const MAP_ANON: c_int = 0x1000;

	pub const MAP_FAILED: *mut c_void = !0 as *mut c_void;

	pub const MCL_CURRENT: c_int = 0x0001;
	pub const MCL_FUTURE: c_int = 0x0002;

	pub const MS_SYNC: c_int = 0x0000;
	pub const MS_ASYNC: c_int = 0x0001;
	pub const MS_INVALIDATE: c_int = 0x0002;

	pub const EPERM: c_int = 1;
	pub const ENOENT: c_int = 2;
	pub const ESRCH: c_int = 3;
	pub const EINTR: c_int = 4;
	pub const EIO: c_int = 5;
	pub const ENXIO: c_int = 6;
	pub const E2BIG: c_int = 7;
	pub const ENOEXEC: c_int = 8;
	pub const EBADF: c_int = 9;
	pub const ECHILD: c_int = 10;
	pub const EDEADLK: c_int = 11;
	pub const ENOMEM: c_int = 12;
	pub const EACCES: c_int = 13;
	pub const EFAULT: c_int = 14;
	pub const ENOTBLK: c_int = 15;
	pub const EBUSY: c_int = 16;
	pub const EEXIST: c_int = 17;
	pub const EXDEV: c_int = 18;
	pub const ENODEV: c_int = 19;
	pub const ENOTDIR: c_int = 20;
	pub const EISDIR: c_int = 21;
	pub const EINVAL: c_int = 22;
	pub const ENFILE: c_int = 23;
	pub const EMFILE: c_int = 24;
	pub const ENOTTY: c_int = 25;
	pub const ETXTBSY: c_int = 26;
	pub const EFBIG: c_int = 27;
	pub const ENOSPC: c_int = 28;
	pub const ESPIPE: c_int = 29;
	pub const EROFS: c_int = 30;
	pub const EMLINK: c_int = 31;
	pub const EPIPE: c_int = 32;
	pub const EDOM: c_int = 33;
	pub const ERANGE: c_int = 34;
	pub const EAGAIN: c_int = 35;
	pub const EWOULDBLOCK: c_int = 35;
	pub const EINPROGRESS: c_int = 36;
	pub const EALREADY: c_int = 37;
	pub const ENOTSOCK: c_int = 38;
	pub const EDESTADDRREQ: c_int = 39;
	pub const EMSGSIZE: c_int = 40;
	pub const EPROTOTYPE: c_int = 41;
	pub const ENOPROTOOPT: c_int = 42;
	pub const EPROTONOSUPPORT: c_int = 43;
	pub const ESOCKTNOSUPPORT: c_int = 44;
	pub const EOPNOTSUPP: c_int = 45;
	pub const EPFNOSUPPORT: c_int = 46;
	pub const EAFNOSUPPORT: c_int = 47;
	pub const EADDRINUSE: c_int = 48;
	pub const EADDRNOTAVAIL: c_int = 49;
	pub const ENETDOWN: c_int = 50;
	pub const ENETUNREACH: c_int = 51;
	pub const ENETRESET: c_int = 52;
	pub const ECONNABORTED: c_int = 53;
	pub const ECONNRESET: c_int = 54;
	pub const ENOBUFS: c_int = 55;
	pub const EISCONN: c_int = 56;
	pub const ENOTCONN: c_int = 57;
	pub const ESHUTDOWN: c_int = 58;
	pub const ETOOMANYREFS: c_int = 59;
	pub const ETIMEDOUT: c_int = 60;
	pub const ECONNREFUSED: c_int = 61;
	pub const ELOOP: c_int = 62;
	pub const ENAMETOOLONG: c_int = 63;
	pub const EHOSTDOWN: c_int = 64;
	pub const EHOSTUNREACH: c_int = 65;
	pub const ENOTEMPTY: c_int = 66;
	pub const EPROCLIM: c_int = 67;
	pub const EUSERS: c_int = 68;
	pub const EDQUOT: c_int = 69;
	pub const ESTALE: c_int = 70;
	pub const EREMOTE: c_int = 71;
	pub const EBADRPC: c_int = 72;
	pub const ERPCMISMATCH: c_int = 73;
	pub const EPROGUNAVAIL: c_int = 74;
	pub const EPROGMISMATCH: c_int = 75;
	pub const EPROCUNAVAIL: c_int = 76;
	pub const ENOLCK: c_int = 77;
	pub const ENOSYS: c_int = 78;
	pub const EFTYPE: c_int = 79;
	pub const EAUTH: c_int = 80;
	pub const ENEEDAUTH: c_int = 81;
	pub const EIDRM: c_int = 82;
	pub const ENOMSG: c_int = 83;
	pub const EOVERFLOW: c_int = 84;
	pub const ECANCELED: c_int = 85;
	pub const EILSEQ: c_int = 86;
	pub const ENOATTR: c_int = 87;
	pub const EDOOFUS: c_int = 88;
	pub const EBADMSG: c_int = 89;
	pub const EMULTIHOP: c_int = 90;
	pub const ENOLINK: c_int = 91;
	pub const EPROTO: c_int = 92;
	pub const ELAST: c_int = 96;

	pub const F_DUPFD: c_int = 0;
	pub const F_GETFD: c_int = 1;
	pub const F_SETFD: c_int = 2;
	pub const F_GETFL: c_int = 3;
	pub const F_SETFL: c_int = 4;

	pub const SIGTRAP: c_int = 5;

	pub const GLOB_APPEND  : c_int = 0x0001;
	pub const GLOB_DOOFFS  : c_int = 0x0002;
	pub const GLOB_ERR     : c_int = 0x0004;
	pub const GLOB_MARK    : c_int = 0x0008;
	pub const GLOB_NOCHECK : c_int = 0x0010;
	pub const GLOB_NOSORT  : c_int = 0x0020;
	pub const GLOB_NOESCAPE: c_int = 0x2000;

	pub const GLOB_NOSPACE : c_int = -1;
	pub const GLOB_ABORTED : c_int = -2;
	pub const GLOB_NOMATCH : c_int = -3;

	pub const POSIX_MADV_NORMAL: c_int = 0;
	pub const POSIX_MADV_RANDOM: c_int = 1;
	pub const POSIX_MADV_SEQUENTIAL: c_int = 2;
	pub const POSIX_MADV_WILLNEED: c_int = 3;
	pub const POSIX_MADV_DONTNEED: c_int = 4;

	pub const _SC_IOV_MAX: c_int = 56;
	pub const _SC_GETGR_R_SIZE_MAX: c_int = 70;
	pub const _SC_GETPW_R_SIZE_MAX: c_int = 71;
	pub const _SC_LOGIN_NAME_MAX: c_int = 73;
	pub const _SC_MQ_PRIO_MAX: c_int = 75;
	pub const _SC_THREAD_ATTR_STACKADDR: c_int = 82;
	pub const _SC_THREAD_ATTR_STACKSIZE: c_int = 83;
	pub const _SC_THREAD_DESTRUCTOR_ITERATIONS: c_int = 85;
	pub const _SC_THREAD_KEYS_MAX: c_int = 86;
	pub const _SC_THREAD_PRIO_INHERIT: c_int = 87;
	pub const _SC_THREAD_PRIO_PROTECT: c_int = 88;
	pub const _SC_THREAD_PRIORITY_SCHEDULING: c_int = 89;
	pub const _SC_THREAD_PROCESS_SHARED: c_int = 90;
	pub const _SC_THREAD_SAFE_FUNCTIONS: c_int = 91;
	pub const _SC_THREAD_STACK_MIN: c_int = 93;
	pub const _SC_THREAD_THREADS_MAX: c_int = 94;
	pub const _SC_THREADS: c_int = 96;
	pub const _SC_TTY_NAME_MAX: c_int = 101;
	pub const _SC_ATEXIT_MAX: c_int = 107;
	pub const _SC_XOPEN_CRYPT: c_int = 108;
	pub const _SC_XOPEN_ENH_I18N: c_int = 109;
	pub const _SC_XOPEN_LEGACY: c_int = 110;
	pub const _SC_XOPEN_REALTIME: c_int = 111;
	pub const _SC_XOPEN_REALTIME_THREADS: c_int = 112;
	pub const _SC_XOPEN_SHM: c_int = 113;
	pub const _SC_XOPEN_UNIX: c_int = 115;
	pub const _SC_XOPEN_VERSION: c_int = 116;
	pub const _SC_XOPEN_XCU_VERSION: c_int = 117;

	pub const PTHREAD_CREATE_JOINABLE: c_int = 0;
	pub const PTHREAD_CREATE_DETACHED: c_int = 1;

	pub const CLOCK_REALTIME: c_int = 0;
	pub const CLOCK_MONOTONIC: c_int = 4;

	pub const RLIMIT_CPU: c_int = 0;
	pub const RLIMIT_FSIZE: c_int = 1;
	pub const RLIMIT_DATA: c_int = 2;
	pub const RLIMIT_STACK: c_int = 3;
	pub const RLIMIT_CORE: c_int = 4;
	pub const RLIMIT_RSS: c_int = 5;
	pub const RLIMIT_MEMLOCK: c_int = 6;
	pub const RLIMIT_NPROC: c_int = 7;
	pub const RLIMIT_NOFILE: c_int = 8;
	pub const RLIMIT_SBSIZE: c_int = 9;
	pub const RLIMIT_VMEM: c_int = 10;
	pub const RLIMIT_AS: c_int = RLIMIT_VMEM;
	pub const RLIMIT_NPTS: c_int = 11;
	pub const RLIMIT_SWAP: c_int = 12;

	pub const RLIM_NLIMITS: rlim_t = 13;
	pub const RLIM_INFINITY: rlim_t = 0x7fff_ffff_ffff_ffff;

	pub const RUSAGE_SELF: c_int = 0;
	pub const RUSAGE_CHILDREN: c_int = -1;
	pub const RUSAGE_THREAD: c_int = 1;

	pub const MADV_NORMAL: c_int = 0;
	pub const MADV_RANDOM: c_int = 1;
	pub const MADV_SEQUENTIAL: c_int = 2;
	pub const MADV_WILLNEED: c_int = 3;
	pub const MADV_DONTNEED: c_int = 4;
	pub const MADV_FREE: c_int = 5;
	pub const MADV_NOSYNC: c_int = 6;
	pub const MADV_AUTOSYNC: c_int = 7;
	pub const MADV_NOCORE: c_int = 8;
	pub const MADV_CORE: c_int = 9;
	pub const MADV_PROTECT: c_int = 10;

	pub const MINCORE_INCORE: c_int =  0x1;
	pub const MINCORE_REFERENCED: c_int = 0x2;
	pub const MINCORE_MODIFIED: c_int = 0x4;
	pub const MINCORE_REFERENCED_OTHER: c_int = 0x8;
	pub const MINCORE_MODIFIED_OTHER: c_int = 0x10;
	pub const MINCORE_SUPER: c_int = 0x20;

	pub const AF_INET: c_int = 2;
	pub const AF_INET6: c_int = 28;
	pub const AF_UNIX: c_int = 1;
	pub const SOCK_STREAM: c_int = 1;
	pub const SOCK_DGRAM: c_int = 2;
	pub const SOCK_RAW: c_int = 3;
	pub const IPPROTO_TCP: c_int = 6;
	pub const IPPROTO_IP: c_int = 0;
	pub const IPPROTO_IPV6: c_int = 41;
	pub const IP_MULTICAST_TTL: c_int = 10;
	pub const IP_MULTICAST_LOOP: c_int = 11;
	pub const IP_TTL: c_int = 4;
	pub const IP_HDRINCL: c_int = 2;
	pub const IP_ADD_MEMBERSHIP: c_int = 12;
	pub const IP_DROP_MEMBERSHIP: c_int = 13;
	pub const IPV6_JOIN_GROUP: c_int = 12;
	pub const IPV6_LEAVE_GROUP: c_int = 13;

	pub const TCP_NODELAY: c_int = 1;
	pub const TCP_KEEPIDLE: c_int = 256;
	pub const SOL_SOCKET: c_int = 0xffff;
	pub const SO_DEBUG: c_int = 0x01;
	pub const SO_ACCEPTCONN: c_int = 0x0002;
	pub const SO_REUSEADDR: c_int = 0x0004;
	pub const SO_KEEPALIVE: c_int = 0x0008;
	pub const SO_DONTROUTE: c_int = 0x0010;
	pub const SO_BROADCAST: c_int = 0x0020;
	pub const SO_USELOOPBACK: c_int = 0x0040;
	pub const SO_LINGER: c_int = 0x0080;
	pub const SO_OOBINLINE: c_int = 0x0100;
	pub const SO_REUSEPORT: c_int = 0x0200;
	pub const SO_SNDBUF: c_int = 0x1001;
	pub const SO_RCVBUF: c_int = 0x1002;
	pub const SO_SNDLOWAT: c_int = 0x1003;
	pub const SO_RCVLOWAT: c_int = 0x1004;
	pub const SO_SNDTIMEO: c_int = 0x1005;
	pub const SO_RCVTIMEO: c_int = 0x1006;
	pub const SO_ERROR: c_int = 0x1007;
	pub const SO_TYPE: c_int = 0x1008;

	pub const IFF_LOOPBACK: c_int = 0x8;

	pub const SHUT_RD: c_int = 0;
	pub const SHUT_WR: c_int = 1;
	pub const SHUT_RDWR: c_int = 2;

	pub const LOCK_SH: c_int = 1;
	pub const LOCK_EX: c_int = 2;
	pub const LOCK_NB: c_int = 4;
	pub const LOCK_UN: c_int = 8;

	pub const O_SYNC: c_int = 128;
	pub const O_NONBLOCK: c_int = 4;
	pub const CTL_KERN: c_int = 1;
	pub const KERN_PROC: c_int = 14;

	pub const MAP_COPY: c_int = 0x0002;
	pub const MAP_RENAME: c_int = 0x0020;
	pub const MAP_NORESERVE: c_int = 0x0040;
	pub const MAP_HASSEMAPHORE: c_int = 0x0200;
	pub const MAP_STACK: c_int = 0x0400;
	pub const MAP_NOSYNC: c_int = 0x0800;
	pub const MAP_NOCORE: c_int = 0x020000;

	pub const IPPROTO_RAW: c_int = 255;

	pub const _SC_ARG_MAX: c_int = 1;
	pub const _SC_CHILD_MAX: c_int = 2;
	pub const _SC_CLK_TCK: c_int = 3;
	pub const _SC_NGROUPS_MAX: c_int = 4;
	pub const _SC_OPEN_MAX: c_int = 5;
	pub const _SC_JOB_CONTROL: c_int = 6;
	pub const _SC_SAVED_IDS: c_int = 7;
	pub const _SC_VERSION: c_int = 8;
	pub const _SC_BC_BASE_MAX: c_int = 9;
	pub const _SC_BC_DIM_MAX: c_int = 10;
	pub const _SC_BC_SCALE_MAX: c_int = 11;
	pub const _SC_BC_STRING_MAX: c_int = 12;
	pub const _SC_COLL_WEIGHTS_MAX: c_int = 13;
	pub const _SC_EXPR_NEST_MAX: c_int = 14;
	pub const _SC_LINE_MAX: c_int = 15;
	pub const _SC_RE_DUP_MAX: c_int = 16;
	pub const _SC_2_VERSION: c_int = 17;
	pub const _SC_2_C_BIND: c_int = 18;
	pub const _SC_2_C_DEV: c_int = 19;
	pub const _SC_2_CHAR_TERM: c_int = 20;
	pub const _SC_2_FORT_DEV: c_int = 21;
	pub const _SC_2_FORT_RUN: c_int = 22;
	pub const _SC_2_LOCALEDEF: c_int = 23;
	pub const _SC_2_SW_DEV: c_int = 24;
	pub const _SC_2_UPE: c_int = 25;
	pub const _SC_STREAM_MAX: c_int = 26;
	pub const _SC_TZNAME_MAX: c_int = 27;
	pub const _SC_ASYNCHRONOUS_IO: c_int = 28;
	pub const _SC_MAPPED_FILES: c_int = 29;
	pub const _SC_MEMLOCK: c_int = 30;
	pub const _SC_MEMLOCK_RANGE: c_int = 31;
	pub const _SC_MEMORY_PROTECTION: c_int = 32;
	pub const _SC_MESSAGE_PASSING: c_int = 33;
	pub const _SC_PRIORITIZED_IO: c_int = 34;
	pub const _SC_PRIORITY_SCHEDULING: c_int = 35;
	pub const _SC_REALTIME_SIGNALS: c_int = 36;
	pub const _SC_SEMAPHORES: c_int = 37;
	pub const _SC_FSYNC: c_int = 38;
	pub const _SC_SHARED_MEMORY_OBJECTS: c_int = 39;
	pub const _SC_SYNCHRONIZED_IO: c_int = 40;
	pub const _SC_TIMERS: c_int = 41;
	pub const _SC_AIO_LISTIO_MAX: c_int = 42;
	pub const _SC_AIO_MAX: c_int = 43;
	pub const _SC_AIO_PRIO_DELTA_MAX: c_int = 44;
	pub const _SC_DELAYTIMER_MAX: c_int = 45;
	pub const _SC_MQ_OPEN_MAX: c_int = 46;
	pub const _SC_PAGESIZE: c_int = 47;
	pub const _SC_RTSIG_MAX: c_int = 48;
	pub const _SC_SEM_NSEMS_MAX: c_int = 49;
	pub const _SC_SEM_VALUE_MAX: c_int = 50;
	pub const _SC_SIGQUEUE_MAX: c_int = 51;
	pub const _SC_TIMER_MAX: c_int = 52;

	pub const PTHREAD_MUTEX_INITIALIZER: pthread_mutex_t = 0 as *mut _;
	pub const PTHREAD_COND_INITIALIZER: pthread_cond_t = 0 as *mut _;
	pub const PTHREAD_RWLOCK_INITIALIZER: pthread_rwlock_t = 0 as *mut _;
	pub const PTHREAD_MUTEX_RECURSIVE: c_int = 2;
}
/* automatically generated by rust-bindgen */

pub type __int128_t = libc::c_void;
pub type __uint128_t = libc::c_void;
pub type __builtin_va_list = [__va_list_tag; 1usize];
pub type __int8_t = libc::c_char;
pub type __uint8_t = libc::c_uchar;
pub type __int16_t = libc::c_short;
pub type __uint16_t = libc::c_ushort;
pub type __int32_t = libc::c_int;
pub type __uint32_t = libc::c_uint;
pub type __int64_t = libc::c_long;
pub type __uint64_t = libc::c_ulong;
pub type __clock_t = __int32_t;
pub type __critical_t = __int64_t;
pub type __double_t = libc::c_double;
pub type __float_t = libc::c_float;
pub type __intfptr_t = __int64_t;
pub type __intptr_t = __int64_t;
pub type __intmax_t = __int64_t;
pub type __int_fast8_t = __int32_t;
pub type __int_fast16_t = __int32_t;
pub type __int_fast32_t = __int32_t;
pub type __int_fast64_t = __int64_t;
pub type __int_least8_t = __int8_t;
pub type __int_least16_t = __int16_t;
pub type __int_least32_t = __int32_t;
pub type __int_least64_t = __int64_t;
pub type __ptrdiff_t = __int64_t;
pub type __register_t = __int64_t;
pub type __segsz_t = __int64_t;
pub type __size_t = __uint64_t;
pub type __ssize_t = __int64_t;
pub type __time_t = __int64_t;
pub type __uintfptr_t = __uint64_t;
pub type __uintptr_t = __uint64_t;
pub type __uintmax_t = __uint64_t;
pub type __uint_fast8_t = __uint32_t;
pub type __uint_fast16_t = __uint32_t;
pub type __uint_fast32_t = __uint32_t;
pub type __uint_fast64_t = __uint64_t;
pub type __uint_least8_t = __uint8_t;
pub type __uint_least16_t = __uint16_t;
pub type __uint_least32_t = __uint32_t;
pub type __uint_least64_t = __uint64_t;
pub type __u_register_t = __uint64_t;
pub type __vm_offset_t = __uint64_t;
pub type __vm_paddr_t = __uint64_t;
pub type __vm_size_t = __uint64_t;
pub type __vm_ooffset_t = __int64_t;
pub type __vm_pindex_t = __uint64_t;
pub type ___wchar_t = libc::c_int;
pub type __va_list = __builtin_va_list;
pub type __gnuc_va_list = __va_list;
pub type __blksize_t = __uint32_t;
pub type __blkcnt_t = __int64_t;
pub type __clockid_t = __int32_t;
pub type __fflags_t = __uint32_t;
pub type __fsblkcnt_t = __uint64_t;
pub type __fsfilcnt_t = __uint64_t;
pub type __gid_t = __uint32_t;
pub type __id_t = __int64_t;
pub type __ino_t = __uint32_t;
pub type __key_t = libc::c_long;
pub type __lwpid_t = __int32_t;
pub type __mode_t = __uint16_t;
pub type __accmode_t = libc::c_int;
pub type __nl_item = libc::c_int;
pub type __nlink_t = __uint16_t;
pub type __off_t = __int64_t;
pub type __pid_t = __int32_t;
pub type __rlim_t = __int64_t;
pub type __sa_family_t = __uint8_t;
pub type __socklen_t = __uint32_t;
pub type __suseconds_t = libc::c_long;
pub enum Struct___timer { }
pub type __timer_t = *mut Struct___timer;
pub enum Struct___mq { }
pub type __mqd_t = *mut Struct___mq;
pub type __uid_t = __uint32_t;
pub type __useconds_t = libc::c_uint;
pub type __cpuwhich_t = libc::c_int;
pub type __cpulevel_t = libc::c_int;
pub type __cpusetid_t = libc::c_int;
pub type __ct_rune_t = libc::c_int;
pub type __rune_t = __ct_rune_t;
pub type __wint_t = __ct_rune_t;
pub type __char16_t = __uint_least16_t;
pub type __char32_t = __uint_least32_t;
pub type __dev_t = __uint32_t;
pub type __fixpt_t = __uint32_t;
#[repr(C)]
#[derive(Copy)]
pub struct Union_Unnamed1 {
    pub _bindgen_data_: [u64; 16usize],
}
impl Union_Unnamed1 {
    pub unsafe fn __mbstate8(&mut self) -> *mut [libc::c_char; 128usize] {
        let raw: *mut u8 = ::core::mem::transmute(&self._bindgen_data_);
        ::core::mem::transmute(raw.offset(0))
    }
    pub unsafe fn _mbstateL(&mut self) -> *mut __int64_t {
        let raw: *mut u8 = ::core::mem::transmute(&self._bindgen_data_);
        ::core::mem::transmute(raw.offset(0))
    }
}
impl ::core::clone::Clone for Union_Unnamed1 {
    fn clone(&self) -> Self { *self }
}
impl ::core::default::Default for Union_Unnamed1 {
    fn default() -> Self { unsafe { ::core::mem::zeroed() } }
}
pub type __mbstate_t = Union_Unnamed1;
pub enum Struct_pthread { }
pub enum Struct_pthread_attr { }
pub enum Struct_pthread_cond { }
pub enum Struct_pthread_cond_attr { }
pub enum Struct_pthread_mutex { }
pub enum Struct_pthread_mutex_attr { }
pub enum Struct_pthread_rwlock { }
pub enum Struct_pthread_rwlockattr { }
pub enum Struct_pthread_barrier { }
pub enum Struct_pthread_barrier_attr { }
pub enum Struct_pthread_spinlock { }
pub type pthread_t = *mut Struct_pthread;
pub type pthread_attr_t = *mut Struct_pthread_attr;
pub type pthread_mutex_t = *mut Struct_pthread_mutex;
pub type pthread_mutexattr_t = *mut Struct_pthread_mutex_attr;
pub type pthread_cond_t = *mut Struct_pthread_cond;
pub type pthread_condattr_t = *mut Struct_pthread_cond_attr;
pub type pthread_key_t = libc::c_int;
pub type pthread_once_t = Struct_pthread_once;
pub type pthread_rwlock_t = *mut Struct_pthread_rwlock;
pub type pthread_rwlockattr_t = *mut Struct_pthread_rwlockattr;
pub type pthread_barrier_t = *mut Struct_pthread_barrier;
pub enum Struct_pthread_barrierattr { }
pub type pthread_barrierattr_t = *mut Struct_pthread_barrierattr;
pub type pthread_spinlock_t = *mut Struct_pthread_spinlock;
pub type pthread_addr_t = *mut libc::c_void;
pub type pthread_startroutine_t =
    ::core::option::Option<extern "C" fn(arg1: *mut libc::c_void)
                              -> *mut libc::c_void>;
#[repr(C)]
#[derive(Copy)]
pub struct Struct_pthread_once {
    pub state: libc::c_int,
    pub mutex: pthread_mutex_t,
}
impl ::core::clone::Clone for Struct_pthread_once {
    fn clone(&self) -> Self { *self }
}
impl ::core::default::Default for Struct_pthread_once {
    fn default() -> Self { unsafe { ::core::mem::zeroed() } }
}
pub type u_char = libc::c_uchar;
pub type u_short = libc::c_ushort;
pub type u_int = libc::c_uint;
pub type u_long = libc::c_ulong;
pub type int8_t = __int8_t;
pub type int16_t = __int16_t;
pub type int32_t = __int32_t;
pub type int64_t = __int64_t;
pub type uint8_t = __uint8_t;
pub type uint16_t = __uint16_t;
pub type uint32_t = __uint32_t;
pub type uint64_t = __uint64_t;
pub type intptr_t = __intptr_t;
pub type uintptr_t = __uintptr_t;
pub type u_int8_t = __uint8_t;
pub type u_int16_t = __uint16_t;
pub type u_int32_t = __uint32_t;
pub type u_int64_t = __uint64_t;
pub type u_quad_t = __uint64_t;
pub type quad_t = __int64_t;
pub type qaddr_t = *mut quad_t;
pub type caddr_t = *mut libc::c_char;
pub type c_caddr_t = *const libc::c_char;
pub type blksize_t = __blksize_t;
pub type cpuwhich_t = __cpuwhich_t;
pub type cpulevel_t = __cpulevel_t;
pub type cpusetid_t = __cpusetid_t;
pub type blkcnt_t = __blkcnt_t;
pub type clock_t = __clock_t;
pub type clockid_t = __clockid_t;
pub type critical_t = __critical_t;
pub type daddr_t = __int64_t;
pub type dev_t = __dev_t;
pub type fflags_t = __fflags_t;
pub type fixpt_t = __fixpt_t;
pub type fsblkcnt_t = __fsblkcnt_t;
pub type fsfilcnt_t = __fsfilcnt_t;
pub type gid_t = __gid_t;
pub type in_addr_t = __uint32_t;
pub type in_port_t = __uint16_t;
pub type id_t = __id_t;
pub type ino_t = __ino_t;
pub type key_t = __key_t;
pub type lwpid_t = __lwpid_t;
pub type mode_t = __mode_t;
pub type accmode_t = __accmode_t;
pub type nlink_t = __nlink_t;
pub type off_t = __off_t;
pub type pid_t = __pid_t;
pub type register_t = __register_t;
pub type rlim_t = __rlim_t;
pub type sbintime_t = __int64_t;
pub type segsz_t = __segsz_t;
pub type size_t = __size_t;
pub type ssize_t = __ssize_t;
pub type suseconds_t = __suseconds_t;
pub type time_t = __time_t;
pub type timer_t = __timer_t;
pub type mqd_t = __mqd_t;
pub type u_register_t = __u_register_t;
pub type uid_t = __uid_t;
pub type useconds_t = __useconds_t;
pub enum Struct_cap_rights { }
pub type cap_rights_t = Struct_cap_rights;
pub type vm_offset_t = __vm_offset_t;
pub type vm_ooffset_t = __vm_ooffset_t;
pub type vm_paddr_t = __vm_paddr_t;
pub type vm_pindex_t = __vm_pindex_t;
pub type vm_size_t = __vm_size_t;
pub type boolean_t = libc::c_int;
pub enum Struct_device { }
pub type device_t = *mut Struct_device;
pub type intfptr_t = __intfptr_t;
pub type intrmask_t = __uint32_t;
pub type uintfptr_t = __uintfptr_t;
pub type uoff_t = __uint64_t;
pub type vm_memattr_t = libc::c_char;
pub enum Struct_vm_page { }
pub type vm_page_t = *mut Struct_vm_page;
pub type _bool = u8;
#[repr(C)]
#[derive(Copy)]
pub struct Struct___sigset {
    pub __bits: [__uint32_t; 4usize],
}
impl ::core::clone::Clone for Struct___sigset {
    fn clone(&self) -> Self { *self }
}
impl ::core::default::Default for Struct___sigset {
    fn default() -> Self { unsafe { ::core::mem::zeroed() } }
}
pub type __sigset_t = Struct___sigset;
#[repr(C)]
#[derive(Copy)]
pub struct Struct_timeval {
    pub tv_sec: time_t,
    pub tv_usec: suseconds_t,
}
impl ::core::clone::Clone for Struct_timeval {
    fn clone(&self) -> Self { *self }
}
impl ::core::default::Default for Struct_timeval {
    fn default() -> Self { unsafe { ::core::mem::zeroed() } }
}
#[repr(C)]
#[derive(Copy)]
pub struct Struct_timespec {
    pub tv_sec: time_t,
    pub tv_nsec: libc::c_long,
}
impl ::core::clone::Clone for Struct_timespec {
    fn clone(&self) -> Self { *self }
}
impl ::core::default::Default for Struct_timespec {
    fn default() -> Self { unsafe { ::core::mem::zeroed() } }
}
#[repr(C)]
#[derive(Copy)]
pub struct Struct_itimerspec {
    pub it_interval: Struct_timespec,
    pub it_value: Struct_timespec,
}
impl ::core::clone::Clone for Struct_itimerspec {
    fn clone(&self) -> Self { *self }
}
impl ::core::default::Default for Struct_itimerspec {
    fn default() -> Self { unsafe { ::core::mem::zeroed() } }
}
pub type __fd_mask = libc::c_ulong;
pub type fd_mask = __fd_mask;
pub type sigset_t = __sigset_t;
#[repr(C)]
#[derive(Copy)]
pub struct Struct_fd_set {
    pub __fds_bits: [__fd_mask; 16usize],
}
impl ::core::clone::Clone for Struct_fd_set {
    fn clone(&self) -> Self { *self }
}
impl ::core::default::Default for Struct_fd_set {
    fn default() -> Self { unsafe { ::core::mem::zeroed() } }
}
pub type fd_set = Struct_fd_set;
pub type Enum_modeventtype = libc::c_uint;
pub const MOD_LOAD: libc::c_uint = 0;
pub const MOD_UNLOAD: libc::c_uint = 1;
pub const MOD_SHUTDOWN: libc::c_uint = 2;
pub const MOD_QUIESCE: libc::c_uint = 3;
pub type modeventtype_t = Enum_modeventtype;
pub enum Struct_module { }
pub type module_t = *mut Struct_module;
pub type modeventhand_t =
    ::core::option::Option<extern "C" fn(arg1: module_t, arg2: libc::c_int,
                                        arg3: *mut libc::c_void)
                              -> libc::c_int>;
#[repr(C)]
#[derive(Copy)]
pub struct Struct_moduledata {
    pub name: *const libc::c_char,
    pub evhand: modeventhand_t,
    pub _priv: *mut libc::c_void,
}
impl ::core::clone::Clone for Struct_moduledata {
    fn clone(&self) -> Self { *self }
}
impl ::core::default::Default for Struct_moduledata {
    fn default() -> Self { unsafe { ::core::mem::zeroed() } }
}
pub type moduledata_t = Struct_moduledata;
#[repr(C)]
#[derive(Copy)]
pub struct Union_modspecific {
    pub _bindgen_data_: [u64; 1usize],
}
impl Union_modspecific {
    pub unsafe fn intval(&mut self) -> *mut libc::c_int {
        let raw: *mut u8 = ::core::mem::transmute(&self._bindgen_data_);
        ::core::mem::transmute(raw.offset(0))
    }
    pub unsafe fn uintval(&mut self) -> *mut u_int {
        let raw: *mut u8 = ::core::mem::transmute(&self._bindgen_data_);
        ::core::mem::transmute(raw.offset(0))
    }
    pub unsafe fn longval(&mut self) -> *mut libc::c_long {
        let raw: *mut u8 = ::core::mem::transmute(&self._bindgen_data_);
        ::core::mem::transmute(raw.offset(0))
    }
    pub unsafe fn ulongval(&mut self) -> *mut u_long {
        let raw: *mut u8 = ::core::mem::transmute(&self._bindgen_data_);
        ::core::mem::transmute(raw.offset(0))
    }
}
impl ::core::clone::Clone for Union_modspecific {
    fn clone(&self) -> Self { *self }
}
impl ::core::default::Default for Union_modspecific {
    fn default() -> Self { unsafe { ::core::mem::zeroed() } }
}
pub type modspecific_t = Union_modspecific;
#[repr(C)]
#[derive(Copy)]
pub struct Struct_mod_depend {
    pub md_ver_minimum: libc::c_int,
    pub md_ver_preferred: libc::c_int,
    pub md_ver_maximum: libc::c_int,
}
impl ::core::clone::Clone for Struct_mod_depend {
    fn clone(&self) -> Self { *self }
}
impl ::core::default::Default for Struct_mod_depend {
    fn default() -> Self { unsafe { ::core::mem::zeroed() } }
}
#[repr(C)]
#[derive(Copy)]
pub struct Struct_mod_version {
    pub mv_version: libc::c_int,
}
impl ::core::clone::Clone for Struct_mod_version {
    fn clone(&self) -> Self { *self }
}
impl ::core::default::Default for Struct_mod_version {
    fn default() -> Self { unsafe { ::core::mem::zeroed() } }
}
#[repr(C)]
#[derive(Copy)]
pub struct Struct_mod_metadata {
    pub md_version: libc::c_int,
    pub md_type: libc::c_int,
    pub md_data: *mut libc::c_void,
    pub md_cval: *const libc::c_char,
}
impl ::core::clone::Clone for Struct_mod_metadata {
    fn clone(&self) -> Self { *self }
}
impl ::core::default::Default for Struct_mod_metadata {
    fn default() -> Self { unsafe { ::core::mem::zeroed() } }
}
pub enum Struct_linker_file { }
#[repr(C)]
#[derive(Copy)]
pub struct Struct_module_stat {
    pub version: libc::c_int,
    pub name: [libc::c_char; 32usize],
    pub refs: libc::c_int,
    pub id: libc::c_int,
    pub data: modspecific_t,
}
impl ::core::clone::Clone for Struct_module_stat {
    fn clone(&self) -> Self { *self }
}
impl ::core::default::Default for Struct_module_stat {
    fn default() -> Self { unsafe { ::core::mem::zeroed() } }
}
pub enum Struct___hack { }
pub enum Struct_region_descriptor { }
#[repr(C, packed)]
#[derive(Copy)]
pub struct Struct_invpcid_descr {
    pub _bindgen_bitfield_1_: uint64_t,
    pub _bindgen_bitfield_2_: uint64_t,
    pub addr: uint64_t,
}
impl ::core::clone::Clone for Struct_invpcid_descr {
    fn clone(&self) -> Self { *self }
}
impl ::core::default::Default for Struct_invpcid_descr {
    fn default() -> Self { unsafe { ::core::mem::zeroed() } }
}
#[repr(C)]
#[derive(Copy)]
pub struct Struct_callout_list {
    pub lh_first: *mut Struct_callout,
}
impl ::core::clone::Clone for Struct_callout_list {
    fn clone(&self) -> Self { *self }
}
impl ::core::default::Default for Struct_callout_list {
    fn default() -> Self { unsafe { ::core::mem::zeroed() } }
}
#[repr(C)]
#[derive(Copy)]
pub struct Struct_callout_slist {
    pub slh_first: *mut Struct_callout,
}
impl ::core::clone::Clone for Struct_callout_slist {
    fn clone(&self) -> Self { *self }
}
impl ::core::default::Default for Struct_callout_slist {
    fn default() -> Self { unsafe { ::core::mem::zeroed() } }
}
#[repr(C)]
#[derive(Copy)]
pub struct Struct_callout_tailq {
    pub tqh_first: *mut Struct_callout,
    pub tqh_last: *mut *mut Struct_callout,
}
impl ::core::clone::Clone for Struct_callout_tailq {
    fn clone(&self) -> Self { *self }
}
impl ::core::default::Default for Struct_callout_tailq {
    fn default() -> Self { unsafe { ::core::mem::zeroed() } }
}
#[repr(C)]
#[derive(Copy)]
pub struct Struct_callout {
    pub c_links: Union_Unnamed2,
    pub c_time: sbintime_t,
    pub c_precision: sbintime_t,
    pub c_arg: *mut libc::c_void,
    pub c_func: ::core::option::Option<extern "C" fn(arg1: *mut libc::c_void)
                                          -> ()>,
    pub c_lock: *mut Struct_lock_object,
    pub c_flags: libc::c_short,
    pub c_iflags: libc::c_short,
    pub c_cpu: libc::c_int,
}
impl ::core::clone::Clone for Struct_callout {
    fn clone(&self) -> Self { *self }
}
impl ::core::default::Default for Struct_callout {
    fn default() -> Self { unsafe { ::core::mem::zeroed() } }
}
#[repr(C)]
#[derive(Copy)]
pub struct Union_Unnamed2 {
    pub _bindgen_data_: [u64; 2usize],
}
impl Union_Unnamed2 {
    pub unsafe fn le(&mut self) -> *mut Struct_Unnamed3 {
        let raw: *mut u8 = ::core::mem::transmute(&self._bindgen_data_);
        ::core::mem::transmute(raw.offset(0))
    }
    pub unsafe fn sle(&mut self) -> *mut Struct_Unnamed4 {
        let raw: *mut u8 = ::core::mem::transmute(&self._bindgen_data_);
        ::core::mem::transmute(raw.offset(0))
    }
    pub unsafe fn tqe(&mut self) -> *mut Struct_Unnamed5 {
        let raw: *mut u8 = ::core::mem::transmute(&self._bindgen_data_);
        ::core::mem::transmute(raw.offset(0))
    }
}
impl ::core::clone::Clone for Union_Unnamed2 {
    fn clone(&self) -> Self { *self }
}
impl ::core::default::Default for Union_Unnamed2 {
    fn default() -> Self { unsafe { ::core::mem::zeroed() } }
}
#[repr(C)]
#[derive(Copy)]
pub struct Struct_Unnamed3 {
    pub le_next: *mut Struct_callout,
    pub le_prev: *mut *mut Struct_callout,
}
impl ::core::clone::Clone for Struct_Unnamed3 {
    fn clone(&self) -> Self { *self }
}
impl ::core::default::Default for Struct_Unnamed3 {
    fn default() -> Self { unsafe { ::core::mem::zeroed() } }
}
#[repr(C)]
#[derive(Copy)]
pub struct Struct_Unnamed4 {
    pub sle_next: *mut Struct_callout,
}
impl ::core::clone::Clone for Struct_Unnamed4 {
    fn clone(&self) -> Self { *self }
}
impl ::core::default::Default for Struct_Unnamed4 {
    fn default() -> Self { unsafe { ::core::mem::zeroed() } }
}
#[repr(C)]
#[derive(Copy)]
pub struct Struct_Unnamed5 {
    pub tqe_next: *mut Struct_callout,
    pub tqe_prev: *mut *mut Struct_callout,
}
impl ::core::clone::Clone for Struct_Unnamed5 {
    fn clone(&self) -> Self { *self }
}
impl ::core::default::Default for Struct_Unnamed5 {
    fn default() -> Self { unsafe { ::core::mem::zeroed() } }
}
#[repr(C)]
#[derive(Copy)]
pub struct Struct_callout_handle {
    pub callout: *mut Struct_callout,
}
impl ::core::clone::Clone for Struct_callout_handle {
    fn clone(&self) -> Self { *self }
}
impl ::core::default::Default for Struct_callout_handle {
    fn default() -> Self { unsafe { ::core::mem::zeroed() } }
}
pub type int_least8_t = __int_least8_t;
pub type int_least16_t = __int_least16_t;
pub type int_least32_t = __int_least32_t;
pub type int_least64_t = __int_least64_t;
pub type uint_least8_t = __uint_least8_t;
pub type uint_least16_t = __uint_least16_t;
pub type uint_least32_t = __uint_least32_t;
pub type uint_least64_t = __uint_least64_t;
pub type int_fast8_t = __int_fast8_t;
pub type int_fast16_t = __int_fast16_t;
pub type int_fast32_t = __int_fast32_t;
pub type int_fast64_t = __int_fast64_t;
pub type uint_fast8_t = __uint_fast8_t;
pub type uint_fast16_t = __uint_fast16_t;
pub type uint_fast32_t = __uint_fast32_t;
pub type uint_fast64_t = __uint_fast64_t;
pub type intmax_t = __intmax_t;
pub type uintmax_t = __uintmax_t;
pub type Enum_VM_GUEST = libc::c_uint;
pub const VM_GUEST_NO: libc::c_uint = 0;
pub const VM_GUEST_VM: libc::c_uint = 1;
pub const VM_GUEST_XEN: libc::c_uint = 2;
pub const VM_GUEST_HV: libc::c_uint = 3;
pub const VM_GUEST_VMWARE: libc::c_uint = 4;
pub const VM_LAST: libc::c_uint = 5;
pub enum Struct_inpcb { }
pub enum Struct_proc { }
pub enum Struct_socket { }
pub enum Struct_thread { }
pub enum Struct_tty { }
pub enum Struct_ucred { }
pub enum Struct__jmp_buf { }
pub enum Struct_trapframe { }
pub enum Struct_eventtimer { }
pub type cpu_tick_f = extern "C" fn() -> libc::c_ulong;
pub type timeout_t = extern "C" fn(arg1: *mut libc::c_void) -> ();
pub enum Struct_root_hold_token { }
pub enum Struct_unrhdr { }
#[repr(C)]
#[derive(Copy)]
pub struct Struct_timezone {
    pub tz_minuteswest: libc::c_int,
    pub tz_dsttime: libc::c_int,
}
impl ::core::clone::Clone for Struct_timezone {
    fn clone(&self) -> Self { *self }
}
impl ::core::default::Default for Struct_timezone {
    fn default() -> Self { unsafe { ::core::mem::zeroed() } }
}
#[repr(C)]
#[derive(Copy)]
pub struct Struct_bintime {
    pub sec: time_t,
    pub frac: uint64_t,
}
impl ::core::clone::Clone for Struct_bintime {
    fn clone(&self) -> Self { *self }
}
impl ::core::default::Default for Struct_bintime {
    fn default() -> Self { unsafe { ::core::mem::zeroed() } }
}
#[repr(C)]
#[derive(Copy)]
pub struct Struct_itimerval {
    pub it_interval: Struct_timeval,
    pub it_value: Struct_timeval,
}
impl ::core::clone::Clone for Struct_itimerval {
    fn clone(&self) -> Self { *self }
}
impl ::core::default::Default for Struct_itimerval {
    fn default() -> Self { unsafe { ::core::mem::zeroed() } }
}
#[repr(C)]
#[derive(Copy)]
pub struct Struct_clockinfo {
    pub hz: libc::c_int,
    pub tick: libc::c_int,
    pub spare: libc::c_int,
    pub stathz: libc::c_int,
    pub profhz: libc::c_int,
}
impl ::core::clone::Clone for Struct_clockinfo {
    fn clone(&self) -> Self { *self }
}
impl ::core::default::Default for Struct_clockinfo {
    fn default() -> Self { unsafe { ::core::mem::zeroed() } }
}
#[repr(C)]
#[derive(Copy)]
pub struct Struct_priority {
    pub pri_class: u_char,
    pub pri_level: u_char,
    pub pri_native: u_char,
    pub pri_user: u_char,
}
impl ::core::clone::Clone for Struct_priority {
    fn clone(&self) -> Self { *self }
}
impl ::core::default::Default for Struct_priority {
    fn default() -> Self { unsafe { ::core::mem::zeroed() } }
}
pub type Enum_sysinit_sub_id = libc::c_uint;
pub const SI_SUB_DUMMY: libc::c_uint = 0;
pub const SI_SUB_DONE: libc::c_uint = 1;
pub const SI_SUB_TUNABLES: libc::c_uint = 7340032;
pub const SI_SUB_COPYRIGHT: libc::c_uint = 8388609;
pub const SI_SUB_SETTINGS: libc::c_uint = 8912896;
pub const SI_SUB_MTX_POOL_STATIC: libc::c_uint = 9437184;
pub const SI_SUB_LOCKMGR: libc::c_uint = 9961472;
pub const SI_SUB_VM: libc::c_uint = 16777216;
pub const SI_SUB_KMEM: libc::c_uint = 25165824;
pub const SI_SUB_KVM_RSRC: libc::c_uint = 27262976;
pub const SI_SUB_HYPERVISOR: libc::c_uint = 27525120;
pub const SI_SUB_WITNESS: libc::c_uint = 27787264;
pub const SI_SUB_MTX_POOL_DYNAMIC: libc::c_uint = 28049408;
pub const SI_SUB_LOCK: libc::c_uint = 28311552;
pub const SI_SUB_EVENTHANDLER: libc::c_uint = 29360128;
pub const SI_SUB_VNET_PRELINK: libc::c_uint = 31457280;
pub const SI_SUB_KLD: libc::c_uint = 33554432;
pub const SI_SUB_CPU: libc::c_uint = 34603008;
pub const SI_SUB_RACCT: libc::c_uint = 34668544;
pub const SI_SUB_RANDOM: libc::c_uint = 34734080;
pub const SI_SUB_KDTRACE: libc::c_uint = 34865152;
pub const SI_SUB_MAC: libc::c_uint = 35127296;
pub const SI_SUB_MAC_POLICY: libc::c_uint = 35389440;
pub const SI_SUB_MAC_LATE: libc::c_uint = 35454976;
pub const SI_SUB_VNET: libc::c_uint = 35520512;
pub const SI_SUB_INTRINSIC: libc::c_uint = 35651584;
pub const SI_SUB_VM_CONF: libc::c_uint = 36700160;
pub const SI_SUB_DDB_SERVICES: libc::c_uint = 37224448;
pub const SI_SUB_RUN_QUEUE: libc::c_uint = 37748736;
pub const SI_SUB_KTRACE: libc::c_uint = 38273024;
pub const SI_SUB_OPENSOLARIS: libc::c_uint = 38338560;
pub const SI_SUB_CYCLIC: libc::c_uint = 38404096;
pub const SI_SUB_AUDIT: libc::c_uint = 38535168;
pub const SI_SUB_CREATE_INIT: libc::c_uint = 38797312;
pub const SI_SUB_SCHED_IDLE: libc::c_uint = 39845888;
pub const SI_SUB_MBUF: libc::c_uint = 40894464;
pub const SI_SUB_INTR: libc::c_uint = 41943040;
pub const SI_SUB_SOFTINTR: libc::c_uint = 41943041;
pub const SI_SUB_ACL: libc::c_uint = 42991616;
pub const SI_SUB_DEVFS: libc::c_uint = 49283072;
pub const SI_SUB_INIT_IF: libc::c_uint = 50331648;
pub const SI_SUB_NETGRAPH: libc::c_uint = 50397184;
pub const SI_SUB_DTRACE: libc::c_uint = 50462720;
pub const SI_SUB_DTRACE_PROVIDER: libc::c_uint = 50626560;
pub const SI_SUB_DTRACE_ANON: libc::c_uint = 50905088;
pub const SI_SUB_DRIVERS: libc::c_uint = 51380224;
pub const SI_SUB_CONFIGURE: libc::c_uint = 58720256;
pub const SI_SUB_VFS: libc::c_uint = 67108864;
pub const SI_SUB_CLOCKS: libc::c_uint = 75497472;
pub const SI_SUB_CLIST: libc::c_uint = 92274688;
pub const SI_SUB_SYSV_SHM: libc::c_uint = 104857600;
pub const SI_SUB_SYSV_SEM: libc::c_uint = 109051904;
pub const SI_SUB_SYSV_MSG: libc::c_uint = 113246208;
pub const SI_SUB_P1003_1B: libc::c_uint = 115343360;
pub const SI_SUB_PSEUDO: libc::c_uint = 117440512;
pub const SI_SUB_EXEC: libc::c_uint = 121634816;
pub const SI_SUB_PROTO_BEGIN: libc::c_uint = 134217728;
pub const SI_SUB_PROTO_IF: libc::c_uint = 138412032;
pub const SI_SUB_PROTO_DOMAININIT: libc::c_uint = 140509184;
pub const SI_SUB_PROTO_DOMAIN: libc::c_uint = 142606336;
pub const SI_SUB_PROTO_IFATTACHDOMAIN: libc::c_uint = 142606337;
pub const SI_SUB_PROTO_END: libc::c_uint = 150994943;
pub const SI_SUB_KPROF: libc::c_uint = 150994944;
pub const SI_SUB_KICK_SCHEDULER: libc::c_uint = 167772160;
pub const SI_SUB_INT_CONFIG_HOOKS: libc::c_uint = 176160768;
pub const SI_SUB_ROOT_CONF: libc::c_uint = 184549376;
pub const SI_SUB_DUMP_CONF: libc::c_uint = 186646528;
pub const SI_SUB_RAID: libc::c_uint = 188219392;
pub const SI_SUB_SWAP: libc::c_uint = 201326592;
pub const SI_SUB_INTRINSIC_POST: libc::c_uint = 218103808;
pub const SI_SUB_SYSCALLS: libc::c_uint = 226492416;
pub const SI_SUB_VNET_DONE: libc::c_uint = 230686720;
pub const SI_SUB_KTHREAD_INIT: libc::c_uint = 234881024;
pub const SI_SUB_KTHREAD_PAGE: libc::c_uint = 239075328;
pub const SI_SUB_KTHREAD_VM: libc::c_uint = 243269632;
pub const SI_SUB_KTHREAD_BUF: libc::c_uint = 245366784;
pub const SI_SUB_KTHREAD_UPDATE: libc::c_uint = 247463936;
pub const SI_SUB_KTHREAD_IDLE: libc::c_uint = 249561088;
pub const SI_SUB_SMP: libc::c_uint = 251658240;
pub const SI_SUB_RACCTD: libc::c_uint = 252706816;
pub const SI_SUB_LAST: libc::c_uint = 268435455;
pub type Enum_sysinit_elem_order = libc::c_uint;
pub const SI_ORDER_FIRST: libc::c_uint = 0;
pub const SI_ORDER_SECOND: libc::c_uint = 1;
pub const SI_ORDER_THIRD: libc::c_uint = 2;
pub const SI_ORDER_FOURTH: libc::c_uint = 3;
pub const SI_ORDER_MIDDLE: libc::c_uint = 16777216;
pub const SI_ORDER_ANY: libc::c_uint = 268435455;
pub type sysinit_nfunc_t =
    ::core::option::Option<extern "C" fn(arg1: *mut libc::c_void) -> ()>;
pub type sysinit_cfunc_t =
    ::core::option::Option<extern "C" fn(arg1: *const libc::c_void) -> ()>;
#[repr(C)]
#[derive(Copy)]
pub struct Struct_sysinit {
    pub subsystem: Enum_sysinit_sub_id,
    pub order: Enum_sysinit_elem_order,
    pub func: sysinit_cfunc_t,
    pub udata: *const libc::c_void,
}
impl ::core::clone::Clone for Struct_sysinit {
    fn clone(&self) -> Self { *self }
}
impl ::core::default::Default for Struct_sysinit {
    fn default() -> Self { unsafe { ::core::mem::zeroed() } }
}
#[repr(C)]
#[derive(Copy)]
pub struct Struct_tunable_int {
    pub path: *const libc::c_char,
    pub var: *mut libc::c_int,
}
impl ::core::clone::Clone for Struct_tunable_int {
    fn clone(&self) -> Self { *self }
}
impl ::core::default::Default for Struct_tunable_int {
    fn default() -> Self { unsafe { ::core::mem::zeroed() } }
}
#[repr(C)]
#[derive(Copy)]
pub struct Struct_tunable_long {
    pub path: *const libc::c_char,
    pub var: *mut libc::c_long,
}
impl ::core::clone::Clone for Struct_tunable_long {
    fn clone(&self) -> Self { *self }
}
impl ::core::default::Default for Struct_tunable_long {
    fn default() -> Self { unsafe { ::core::mem::zeroed() } }
}
#[repr(C)]
#[derive(Copy)]
pub struct Struct_tunable_ulong {
    pub path: *const libc::c_char,
    pub var: *mut libc::c_ulong,
}
impl ::core::clone::Clone for Struct_tunable_ulong {
    fn clone(&self) -> Self { *self }
}
impl ::core::default::Default for Struct_tunable_ulong {
    fn default() -> Self { unsafe { ::core::mem::zeroed() } }
}
#[repr(C)]
#[derive(Copy)]
pub struct Struct_tunable_quad {
    pub path: *const libc::c_char,
    pub var: *mut quad_t,
}
impl ::core::clone::Clone for Struct_tunable_quad {
    fn clone(&self) -> Self { *self }
}
impl ::core::default::Default for Struct_tunable_quad {
    fn default() -> Self { unsafe { ::core::mem::zeroed() } }
}
#[repr(C)]
#[derive(Copy)]
pub struct Struct_tunable_str {
    pub path: *const libc::c_char,
    pub var: *mut libc::c_char,
    pub size: libc::c_int,
}
impl ::core::clone::Clone for Struct_tunable_str {
    fn clone(&self) -> Self { *self }
}
impl ::core::default::Default for Struct_tunable_str {
    fn default() -> Self { unsafe { ::core::mem::zeroed() } }
}
#[repr(C)]
#[derive(Copy)]
pub struct Struct_intr_config_hook {
    pub ich_links: Struct_Unnamed6,
    pub ich_func: ::core::option::Option<extern "C" fn(arg:
                                                          *mut libc::c_void)
                                            -> ()>,
    pub ich_arg: *mut libc::c_void,
}
impl ::core::clone::Clone for Struct_intr_config_hook {
    fn clone(&self) -> Self { *self }
}
impl ::core::default::Default for Struct_intr_config_hook {
    fn default() -> Self { unsafe { ::core::mem::zeroed() } }
}
#[repr(C)]
#[derive(Copy)]
pub struct Struct_Unnamed6 {
    pub tqe_next: *mut Struct_intr_config_hook,
    pub tqe_prev: *mut *mut Struct_intr_config_hook,
}
impl ::core::clone::Clone for Struct_Unnamed6 {
    fn clone(&self) -> Self { *self }
}
impl ::core::default::Default for Struct_Unnamed6 {
    fn default() -> Self { unsafe { ::core::mem::zeroed() } }
}
pub enum Struct_witness { }
#[repr(C)]
#[derive(Copy)]
pub struct Struct_lock_object {
    pub lo_name: *const libc::c_char,
    pub lo_flags: u_int,
    pub lo_data: u_int,
    pub lo_witness: *mut Struct_witness,
}
impl ::core::clone::Clone for Struct_lock_object {
    fn clone(&self) -> Self { *self }
}
impl ::core::default::Default for Struct_lock_object {
    fn default() -> Self { unsafe { ::core::mem::zeroed() } }
}
pub enum Struct_lock_list_entry { }
#[repr(C)]
#[derive(Copy)]
pub struct Struct_lock_class {
    pub lc_name: *const libc::c_char,
    pub lc_flags: u_int,
    pub lc_assert: ::core::option::Option<extern "C" fn(lock:
                                                           *const Struct_lock_object,
                                                       what: libc::c_int)
                                             -> ()>,
    pub lc_ddb_show: ::core::option::Option<extern "C" fn(lock:
                                                             *const Struct_lock_object)
                                               -> ()>,
    pub lc_lock: ::core::option::Option<extern "C" fn(lock:
                                                         *mut Struct_lock_object,
                                                     how: uintptr_t) -> ()>,
    pub lc_owner: ::core::option::Option<extern "C" fn(lock:
                                                          *const Struct_lock_object,
                                                      owner:
                                                          *mut *mut Struct_thread)
                                            -> libc::c_int>,
    pub lc_unlock: ::core::option::Option<extern "C" fn(lock:
                                                           *mut Struct_lock_object)
                                             -> uintptr_t>,
}
impl ::core::clone::Clone for Struct_lock_class {
    fn clone(&self) -> Self { *self }
}
impl ::core::default::Default for Struct_lock_class {
    fn default() -> Self { unsafe { ::core::mem::zeroed() } }
}
#[repr(C)]
#[derive(Copy)]
pub struct Struct__cpuset {
    pub __bits: [libc::c_long; 1usize],
}
impl ::core::clone::Clone for Struct__cpuset {
    fn clone(&self) -> Self { *self }
}
impl ::core::default::Default for Struct__cpuset {
    fn default() -> Self { unsafe { ::core::mem::zeroed() } }
}
pub type cpuset_t = Struct__cpuset;
#[repr(C)]
#[derive(Copy)]
pub struct Struct_ktr_entry {
    pub ktr_timestamp: u_int64_t,
    pub ktr_cpu: libc::c_int,
    pub ktr_line: libc::c_int,
    pub ktr_file: *const libc::c_char,
    pub ktr_desc: *const libc::c_char,
    pub ktr_thread: *mut Struct_thread,
    pub ktr_parms: [u_long; 6usize],
}
impl ::core::clone::Clone for Struct_ktr_entry {
    fn clone(&self) -> Self { *self }
}
impl ::core::default::Default for Struct_ktr_entry {
    fn default() -> Self { unsafe { ::core::mem::zeroed() } }
}
#[repr(C)]
#[derive(Copy)]
pub struct Struct_mtx {
    pub lock_object: Struct_lock_object,
    pub mtx_lock: uintptr_t,
}
impl ::core::clone::Clone for Struct_mtx {
    fn clone(&self) -> Self { *self }
}
impl ::core::default::Default for Struct_mtx {
    fn default() -> Self { unsafe { ::core::mem::zeroed() } }
}
#[repr(C)]
#[derive(Copy)]
pub struct Struct_mtx_padalign {
    pub lock_object: Struct_lock_object,
    pub mtx_lock: uintptr_t,
}
impl ::core::clone::Clone for Struct_mtx_padalign {
    fn clone(&self) -> Self { *self }
}
impl ::core::default::Default for Struct_mtx_padalign {
    fn default() -> Self { unsafe { ::core::mem::zeroed() } }
}
#[repr(C)]
#[derive(Copy)]
pub struct Struct_sx {
    pub lock_object: Struct_lock_object,
    pub sx_lock: uintptr_t,
}
impl ::core::clone::Clone for Struct_sx {
    fn clone(&self) -> Self { *self }
}
impl ::core::default::Default for Struct_sx {
    fn default() -> Self { unsafe { ::core::mem::zeroed() } }
}
#[repr(C)]
#[derive(Copy)]
pub struct Struct_rmpriolist {
    pub lh_first: *mut Struct_rm_priotracker,
}
impl ::core::clone::Clone for Struct_rmpriolist {
    fn clone(&self) -> Self { *self }
}
impl ::core::default::Default for Struct_rmpriolist {
    fn default() -> Self { unsafe { ::core::mem::zeroed() } }
}
#[repr(C)]
#[derive(Copy)]
pub struct Struct_rm_queue {
    pub rmq_next: *mut Struct_rm_queue,
    pub rmq_prev: *mut Struct_rm_queue,
}
impl ::core::clone::Clone for Struct_rm_queue {
    fn clone(&self) -> Self { *self }
}
impl ::core::default::Default for Struct_rm_queue {
    fn default() -> Self { unsafe { ::core::mem::zeroed() } }
}
#[repr(C)]
#[derive(Copy)]
pub struct Struct_rmlock {
    pub lock_object: Struct_lock_object,
    pub rm_writecpus: cpuset_t,
    pub rm_activeReaders: Struct_Unnamed7,
    pub _rm_lock: Union_Unnamed8,
}
impl ::core::clone::Clone for Struct_rmlock {
    fn clone(&self) -> Self { *self }
}
impl ::core::default::Default for Struct_rmlock {
    fn default() -> Self { unsafe { ::core::mem::zeroed() } }
}
#[repr(C)]
#[derive(Copy)]
pub struct Struct_Unnamed7 {
    pub lh_first: *mut Struct_rm_priotracker,
}
impl ::core::clone::Clone for Struct_Unnamed7 {
    fn clone(&self) -> Self { *self }
}
impl ::core::default::Default for Struct_Unnamed7 {
    fn default() -> Self { unsafe { ::core::mem::zeroed() } }
}
#[repr(C)]
#[derive(Copy)]
pub struct Union_Unnamed8 {
    pub _bindgen_data_: [u64; 4usize],
}
impl Union_Unnamed8 {
    pub unsafe fn _rm_wlock_object(&mut self) -> *mut Struct_lock_object {
        let raw: *mut u8 = ::core::mem::transmute(&self._bindgen_data_);
        ::core::mem::transmute(raw.offset(0))
    }
    pub unsafe fn _rm_lock_mtx(&mut self) -> *mut Struct_mtx {
        let raw: *mut u8 = ::core::mem::transmute(&self._bindgen_data_);
        ::core::mem::transmute(raw.offset(0))
    }
    pub unsafe fn _rm_lock_sx(&mut self) -> *mut Struct_sx {
        let raw: *mut u8 = ::core::mem::transmute(&self._bindgen_data_);
        ::core::mem::transmute(raw.offset(0))
    }
}
impl ::core::clone::Clone for Union_Unnamed8 {
    fn clone(&self) -> Self { *self }
}
impl ::core::default::Default for Union_Unnamed8 {
    fn default() -> Self { unsafe { ::core::mem::zeroed() } }
}
#[repr(C)]
#[derive(Copy)]
pub struct Struct_rm_priotracker {
    pub rmp_cpuQueue: Struct_rm_queue,
    pub rmp_rmlock: *mut Struct_rmlock,
    pub rmp_thread: *mut Struct_thread,
    pub rmp_flags: libc::c_int,
    pub rmp_qentry: Struct_Unnamed9,
}
impl ::core::clone::Clone for Struct_rm_priotracker {
    fn clone(&self) -> Self { *self }
}
impl ::core::default::Default for Struct_rm_priotracker {
    fn default() -> Self { unsafe { ::core::mem::zeroed() } }
}
#[repr(C)]
#[derive(Copy)]
pub struct Struct_Unnamed9 {
    pub le_next: *mut Struct_rm_priotracker,
    pub le_prev: *mut *mut Struct_rm_priotracker,
}
impl ::core::clone::Clone for Struct_Unnamed9 {
    fn clone(&self) -> Self { *self }
}
impl ::core::default::Default for Struct_Unnamed9 {
    fn default() -> Self { unsafe { ::core::mem::zeroed() } }
}
#[repr(C)]
#[derive(Copy)]
pub struct Struct_vmmeter {
    pub v_swtch: u_int,
    pub v_trap: u_int,
    pub v_syscall: u_int,
    pub v_intr: u_int,
    pub v_soft: u_int,
    pub v_vm_faults: u_int,
    pub v_io_faults: u_int,
    pub v_cow_faults: u_int,
    pub v_cow_optim: u_int,
    pub v_zfod: u_int,
    pub v_ozfod: u_int,
    pub v_swapin: u_int,
    pub v_swapout: u_int,
    pub v_swappgsin: u_int,
    pub v_swappgsout: u_int,
    pub v_vnodein: u_int,
    pub v_vnodeout: u_int,
    pub v_vnodepgsin: u_int,
    pub v_vnodepgsout: u_int,
    pub v_intrans: u_int,
    pub v_reactivated: u_int,
    pub v_pdwakeups: u_int,
    pub v_pdpages: u_int,
    pub v_tcached: u_int,
    pub v_dfree: u_int,
    pub v_pfree: u_int,
    pub v_tfree: u_int,
    pub v_page_size: u_int,
    pub v_page_count: u_int,
    pub v_free_reserved: u_int,
    pub v_free_target: u_int,
    pub v_free_min: u_int,
    pub v_free_count: u_int,
    pub v_wire_count: u_int,
    pub v_active_count: u_int,
    pub v_inactive_target: u_int,
    pub v_inactive_count: u_int,
    pub v_cache_count: u_int,
    pub v_cache_min: u_int,
    pub v_cache_max: u_int,
    pub v_pageout_free_min: u_int,
    pub v_interrupt_free_min: u_int,
    pub v_free_severe: u_int,
    pub v_forks: u_int,
    pub v_vforks: u_int,
    pub v_rforks: u_int,
    pub v_kthreads: u_int,
    pub v_forkpages: u_int,
    pub v_vforkpages: u_int,
    pub v_rforkpages: u_int,
    pub v_kthreadpages: u_int,
}
impl ::core::clone::Clone for Struct_vmmeter {
    fn clone(&self) -> Self { *self }
}
impl ::core::default::Default for Struct_vmmeter {
    fn default() -> Self { unsafe { ::core::mem::zeroed() } }
}
#[repr(C)]
#[derive(Copy)]
pub struct Struct_vmtotal {
    pub t_rq: int16_t,
    pub t_dw: int16_t,
    pub t_pw: int16_t,
    pub t_sl: int16_t,
    pub t_sw: int16_t,
    pub t_vm: int32_t,
    pub t_avm: int32_t,
    pub t_rm: int32_t,
    pub t_arm: int32_t,
    pub t_vmshr: int32_t,
    pub t_avmshr: int32_t,
    pub t_rmshr: int32_t,
    pub t_armshr: int32_t,
    pub t_free: int32_t,
}
impl ::core::clone::Clone for Struct_vmtotal {
    fn clone(&self) -> Self { *self }
}
impl ::core::default::Default for Struct_vmtotal {
    fn default() -> Self { unsafe { ::core::mem::zeroed() } }
}
#[repr(C)]
#[derive(Copy)]
pub struct Struct_rusage {
    pub ru_utime: Struct_timeval,
    pub ru_stime: Struct_timeval,
    pub ru_maxrss: libc::c_long,
    pub ru_ixrss: libc::c_long,
    pub ru_idrss: libc::c_long,
    pub ru_isrss: libc::c_long,
    pub ru_minflt: libc::c_long,
    pub ru_majflt: libc::c_long,
    pub ru_nswap: libc::c_long,
    pub ru_inblock: libc::c_long,
    pub ru_oublock: libc::c_long,
    pub ru_msgsnd: libc::c_long,
    pub ru_msgrcv: libc::c_long,
    pub ru_nsignals: libc::c_long,
    pub ru_nvcsw: libc::c_long,
    pub ru_nivcsw: libc::c_long,
}
impl ::core::clone::Clone for Struct_rusage {
    fn clone(&self) -> Self { *self }
}
impl ::core::default::Default for Struct_rusage {
    fn default() -> Self { unsafe { ::core::mem::zeroed() } }
}
#[repr(C)]
#[derive(Copy)]
pub struct Struct___wrusage {
    pub wru_self: Struct_rusage,
    pub wru_children: Struct_rusage,
}
impl ::core::clone::Clone for Struct___wrusage {
    fn clone(&self) -> Self { *self }
}
impl ::core::default::Default for Struct___wrusage {
    fn default() -> Self { unsafe { ::core::mem::zeroed() } }
}
#[repr(C)]
#[derive(Copy)]
pub struct Struct_rlimit {
    pub rlim_cur: rlim_t,
    pub rlim_max: rlim_t,
}
impl ::core::clone::Clone for Struct_rlimit {
    fn clone(&self) -> Self { *self }
}
impl ::core::default::Default for Struct_rlimit {
    fn default() -> Self { unsafe { ::core::mem::zeroed() } }
}
#[repr(C)]
#[derive(Copy)]
pub struct Struct_orlimit {
    pub rlim_cur: __int32_t,
    pub rlim_max: __int32_t,
}
impl ::core::clone::Clone for Struct_orlimit {
    fn clone(&self) -> Self { *self }
}
impl ::core::default::Default for Struct_orlimit {
    fn default() -> Self { unsafe { ::core::mem::zeroed() } }
}
#[repr(C)]
#[derive(Copy)]
pub struct Struct_loadavg {
    pub ldavg: [__fixpt_t; 3usize],
    pub fscale: libc::c_long,
}
impl ::core::clone::Clone for Struct_loadavg {
    fn clone(&self) -> Self { *self }
}
impl ::core::default::Default for Struct_loadavg {
    fn default() -> Self { unsafe { ::core::mem::zeroed() } }
}
pub enum Struct_pcb { }
pub enum Struct_pmap { }
pub enum Struct_amd64tss { }
pub enum Struct_user_segment_descriptor { }
pub enum Struct_system_segment_descriptor { }
#[repr(C)]
#[derive(Copy)]
pub struct Struct_pcpu {
    pub pc_curthread: *mut Struct_thread,
    pub pc_idlethread: *mut Struct_thread,
    pub pc_fpcurthread: *mut Struct_thread,
    pub pc_deadthread: *mut Struct_thread,
    pub pc_curpcb: *mut Struct_pcb,
    pub pc_switchtime: uint64_t,
    pub pc_switchticks: libc::c_int,
    pub pc_cpuid: u_int,
    pub pc_allcpu: Struct_Unnamed10,
    pub pc_spinlocks: *mut Struct_lock_list_entry,
    pub pc_cnt: Struct_vmmeter,
    pub pc_cp_time: [libc::c_long; 5usize],
    pub pc_device: *mut Struct_device,
    pub pc_netisr: *mut libc::c_void,
    pub pc_dnweight: libc::c_int,
    pub pc_domain: libc::c_int,
    pub pc_rm_queue: Struct_rm_queue,
    pub pc_dynamic: uintptr_t,
    pub pc_monitorbuf: [libc::c_char; 128usize],
    pub pc_prvspace: *mut Struct_pcpu,
    pub pc_curpmap: *mut Struct_pmap,
    pub pc_tssp: *mut Struct_amd64tss,
    pub pc_commontssp: *mut Struct_amd64tss,
    pub pc_rsp0: register_t,
    pub pc_scratch_rsp: register_t,
    pub pc_apic_id: u_int,
    pub pc_acpi_id: u_int,
    pub pc_fs32p: *mut Struct_user_segment_descriptor,
    pub pc_gs32p: *mut Struct_user_segment_descriptor,
    pub pc_ldt: *mut Struct_system_segment_descriptor,
    pub pc_tss: *mut Struct_system_segment_descriptor,
    pub pc_pm_save_cnt: uint64_t,
    pub pc_cmci_mask: u_int,
    pub pc_dbreg: [uint64_t; 16usize],
    pub pc_dbreg_cmd: libc::c_int,
    pub pc_vcpu_id: u_int,
    pub __pad: [libc::c_char; 157usize],
}
impl ::core::clone::Clone for Struct_pcpu {
    fn clone(&self) -> Self { *self }
}
impl ::core::default::Default for Struct_pcpu {
    fn default() -> Self { unsafe { ::core::mem::zeroed() } }
}
#[repr(C)]
#[derive(Copy)]
pub struct Struct_Unnamed10 {
    pub stqe_next: *mut Struct_pcpu,
}
impl ::core::clone::Clone for Struct_Unnamed10 {
    fn clone(&self) -> Self { *self }
}
impl ::core::default::Default for Struct_Unnamed10 {
    fn default() -> Self { unsafe { ::core::mem::zeroed() } }
}
#[repr(C)]
#[derive(Copy)]
pub struct Struct_cpuhead {
    pub stqh_first: *mut Struct_pcpu,
    pub stqh_last: *mut *mut Struct_pcpu,
}
impl ::core::clone::Clone for Struct_cpuhead {
    fn clone(&self) -> Self { *self }
}
impl ::core::default::Default for Struct_cpuhead {
    fn default() -> Self { unsafe { ::core::mem::zeroed() } }
}
pub enum Struct_lock_profile_object { }
#[repr(C)]
#[derive(Copy)]
pub struct Struct_lpohead {
    pub lh_first: *mut Struct_lock_profile_object,
}
impl ::core::clone::Clone for Struct_lpohead {
    fn clone(&self) -> Self { *self }
}
impl ::core::default::Default for Struct_lpohead {
    fn default() -> Self { unsafe { ::core::mem::zeroed() } }
}
pub type lockstat_probe_func_t =
    ::core::option::Option<extern "C" fn(arg1: uint32_t, arg0: uintptr_t,
                                        arg1: uintptr_t, arg2: uintptr_t,
                                        arg3: uintptr_t, arg4: uintptr_t)
                              -> ()>;
pub enum Struct_mtx_pool { }
#[repr(C)]
#[derive(Copy)]
pub struct Struct_mtx_args {
    pub ma_mtx: *mut libc::c_void,
    pub ma_desc: *const libc::c_char,
    pub ma_opts: libc::c_int,
}
impl ::core::clone::Clone for Struct_mtx_args {
    fn clone(&self) -> Self { *self }
}
impl ::core::default::Default for Struct_mtx_args {
    fn default() -> Self { unsafe { ::core::mem::zeroed() } }
}
#[repr(C)]
#[derive(Copy)]
pub struct Struct_eventhandler_entry {
    pub ee_link: Struct_Unnamed11,
    pub ee_priority: libc::c_int,
    pub ee_arg: *mut libc::c_void,
}
impl ::core::clone::Clone for Struct_eventhandler_entry {
    fn clone(&self) -> Self { *self }
}
impl ::core::default::Default for Struct_eventhandler_entry {
    fn default() -> Self { unsafe { ::core::mem::zeroed() } }
}
#[repr(C)]
#[derive(Copy)]
pub struct Struct_Unnamed11 {
    pub tqe_next: *mut Struct_eventhandler_entry,
    pub tqe_prev: *mut *mut Struct_eventhandler_entry,
}
impl ::core::clone::Clone for Struct_Unnamed11 {
    fn clone(&self) -> Self { *self }
}
impl ::core::default::Default for Struct_Unnamed11 {
    fn default() -> Self { unsafe { ::core::mem::zeroed() } }
}
#[repr(C)]
#[derive(Copy)]
pub struct Struct_eventhandler_list {
    pub el_name: *mut libc::c_char,
    pub el_flags: libc::c_int,
    pub el_runcount: u_int,
    pub el_lock: Struct_mtx,
    pub el_link: Struct_Unnamed12,
    pub el_entries: Struct_Unnamed13,
}
impl ::core::clone::Clone for Struct_eventhandler_list {
    fn clone(&self) -> Self { *self }
}
impl ::core::default::Default for Struct_eventhandler_list {
    fn default() -> Self { unsafe { ::core::mem::zeroed() } }
}
#[repr(C)]
#[derive(Copy)]
pub struct Struct_Unnamed12 {
    pub tqe_next: *mut Struct_eventhandler_list,
    pub tqe_prev: *mut *mut Struct_eventhandler_list,
}
impl ::core::clone::Clone for Struct_Unnamed12 {
    fn clone(&self) -> Self { *self }
}
impl ::core::default::Default for Struct_Unnamed12 {
    fn default() -> Self { unsafe { ::core::mem::zeroed() } }
}
#[repr(C)]
#[derive(Copy)]
pub struct Struct_Unnamed13 {
    pub tqh_first: *mut Struct_eventhandler_entry,
    pub tqh_last: *mut *mut Struct_eventhandler_entry,
}
impl ::core::clone::Clone for Struct_Unnamed13 {
    fn clone(&self) -> Self { *self }
}
impl ::core::default::Default for Struct_Unnamed13 {
    fn default() -> Self { unsafe { ::core::mem::zeroed() } }
}
pub type eventhandler_tag = *mut Struct_eventhandler_entry;
pub type shutdown_fn =
    ::core::option::Option<extern "C" fn(arg1: *mut libc::c_void,
                                        arg2: libc::c_int) -> ()>;
#[repr(C)]
#[derive(Copy)]
pub struct Struct_eventhandler_entry_shutdown_pre_sync {
    pub ee: Struct_eventhandler_entry,
    pub eh_func: shutdown_fn,
}
impl ::core::clone::Clone for Struct_eventhandler_entry_shutdown_pre_sync {
    fn clone(&self) -> Self { *self }
}
impl ::core::default::Default for Struct_eventhandler_entry_shutdown_pre_sync {
    fn default() -> Self { unsafe { ::core::mem::zeroed() } }
}
#[repr(C)]
#[derive(Copy)]
pub struct Struct_eventhandler_entry_shutdown_post_sync {
    pub ee: Struct_eventhandler_entry,
    pub eh_func: shutdown_fn,
}
impl ::core::clone::Clone for Struct_eventhandler_entry_shutdown_post_sync {
    fn clone(&self) -> Self { *self }
}
impl ::core::default::Default for Struct_eventhandler_entry_shutdown_post_sync
 {
    fn default() -> Self { unsafe { ::core::mem::zeroed() } }
}
#[repr(C)]
#[derive(Copy)]
pub struct Struct_eventhandler_entry_shutdown_final {
    pub ee: Struct_eventhandler_entry,
    pub eh_func: shutdown_fn,
}
impl ::core::clone::Clone for Struct_eventhandler_entry_shutdown_final {
    fn clone(&self) -> Self { *self }
}
impl ::core::default::Default for Struct_eventhandler_entry_shutdown_final {
    fn default() -> Self { unsafe { ::core::mem::zeroed() } }
}
pub type power_change_fn =
    ::core::option::Option<extern "C" fn(arg1: *mut libc::c_void) -> ()>;
#[repr(C)]
#[derive(Copy)]
pub struct Struct_eventhandler_entry_power_resume {
    pub ee: Struct_eventhandler_entry,
    pub eh_func: power_change_fn,
}
impl ::core::clone::Clone for Struct_eventhandler_entry_power_resume {
    fn clone(&self) -> Self { *self }
}
impl ::core::default::Default for Struct_eventhandler_entry_power_resume {
    fn default() -> Self { unsafe { ::core::mem::zeroed() } }
}
#[repr(C)]
#[derive(Copy)]
pub struct Struct_eventhandler_entry_power_suspend {
    pub ee: Struct_eventhandler_entry,
    pub eh_func: power_change_fn,
}
impl ::core::clone::Clone for Struct_eventhandler_entry_power_suspend {
    fn clone(&self) -> Self { *self }
}
impl ::core::default::Default for Struct_eventhandler_entry_power_suspend {
    fn default() -> Self { unsafe { ::core::mem::zeroed() } }
}
#[repr(C)]
#[derive(Copy)]
pub struct Struct_eventhandler_entry_power_suspend_early {
    pub ee: Struct_eventhandler_entry,
    pub eh_func: power_change_fn,
}
impl ::core::clone::Clone for Struct_eventhandler_entry_power_suspend_early {
    fn clone(&self) -> Self { *self }
}
impl ::core::default::Default for Struct_eventhandler_entry_power_suspend_early
 {
    fn default() -> Self { unsafe { ::core::mem::zeroed() } }
}
pub type vm_lowmem_handler_t =
    ::core::option::Option<extern "C" fn(arg1: *mut libc::c_void,
                                        arg2: libc::c_int) -> ()>;
#[repr(C)]
#[derive(Copy)]
pub struct Struct_eventhandler_entry_vm_lowmem {
    pub ee: Struct_eventhandler_entry,
    pub eh_func: vm_lowmem_handler_t,
}
impl ::core::clone::Clone for Struct_eventhandler_entry_vm_lowmem {
    fn clone(&self) -> Self { *self }
}
impl ::core::default::Default for Struct_eventhandler_entry_vm_lowmem {
    fn default() -> Self { unsafe { ::core::mem::zeroed() } }
}
pub type mountroot_handler_t =
    ::core::option::Option<extern "C" fn(arg1: *mut libc::c_void) -> ()>;
#[repr(C)]
#[derive(Copy)]
pub struct Struct_eventhandler_entry_mountroot {
    pub ee: Struct_eventhandler_entry,
    pub eh_func: mountroot_handler_t,
}
impl ::core::clone::Clone for Struct_eventhandler_entry_mountroot {
    fn clone(&self) -> Self { *self }
}
impl ::core::default::Default for Struct_eventhandler_entry_mountroot {
    fn default() -> Self { unsafe { ::core::mem::zeroed() } }
}
pub enum Struct_mount { }
pub enum Struct_vnode { }
pub type vfs_mounted_notify_fn =
    ::core::option::Option<extern "C" fn(arg1: *mut libc::c_void,
                                        arg2: *mut Struct_mount,
                                        arg3: *mut Struct_vnode,
                                        arg4: *mut Struct_thread) -> ()>;
pub type vfs_unmounted_notify_fn =
    ::core::option::Option<extern "C" fn(arg1: *mut libc::c_void,
                                        arg2: *mut Struct_mount,
                                        arg3: *mut Struct_thread) -> ()>;
#[repr(C)]
#[derive(Copy)]
pub struct Struct_eventhandler_entry_vfs_mounted {
    pub ee: Struct_eventhandler_entry,
    pub eh_func: vfs_mounted_notify_fn,
}
impl ::core::clone::Clone for Struct_eventhandler_entry_vfs_mounted {
    fn clone(&self) -> Self { *self }
}
impl ::core::default::Default for Struct_eventhandler_entry_vfs_mounted {
    fn default() -> Self { unsafe { ::core::mem::zeroed() } }
}
#[repr(C)]
#[derive(Copy)]
pub struct Struct_eventhandler_entry_vfs_unmounted {
    pub ee: Struct_eventhandler_entry,
    pub eh_func: vfs_unmounted_notify_fn,
}
impl ::core::clone::Clone for Struct_eventhandler_entry_vfs_unmounted {
    fn clone(&self) -> Self { *self }
}
impl ::core::default::Default for Struct_eventhandler_entry_vfs_unmounted {
    fn default() -> Self { unsafe { ::core::mem::zeroed() } }
}
pub enum Struct_ifnet { }
pub type vlan_config_fn =
    ::core::option::Option<extern "C" fn(arg1: *mut libc::c_void,
                                        arg2: *mut Struct_ifnet,
                                        arg3: uint16_t) -> ()>;
pub type vlan_unconfig_fn =
    ::core::option::Option<extern "C" fn(arg1: *mut libc::c_void,
                                        arg2: *mut Struct_ifnet,
                                        arg3: uint16_t) -> ()>;
#[repr(C)]
#[derive(Copy)]
pub struct Struct_eventhandler_entry_vlan_config {
    pub ee: Struct_eventhandler_entry,
    pub eh_func: vlan_config_fn,
}
impl ::core::clone::Clone for Struct_eventhandler_entry_vlan_config {
    fn clone(&self) -> Self { *self }
}
impl ::core::default::Default for Struct_eventhandler_entry_vlan_config {
    fn default() -> Self { unsafe { ::core::mem::zeroed() } }
}
#[repr(C)]
#[derive(Copy)]
pub struct Struct_eventhandler_entry_vlan_unconfig {
    pub ee: Struct_eventhandler_entry,
    pub eh_func: vlan_unconfig_fn,
}
impl ::core::clone::Clone for Struct_eventhandler_entry_vlan_unconfig {
    fn clone(&self) -> Self { *self }
}
impl ::core::default::Default for Struct_eventhandler_entry_vlan_unconfig {
    fn default() -> Self { unsafe { ::core::mem::zeroed() } }
}
pub type bpf_track_fn =
    ::core::option::Option<extern "C" fn(arg1: *mut libc::c_void,
                                        arg2: *mut Struct_ifnet,
                                        arg3: libc::c_int,
                                        arg4: libc::c_int) -> ()>;
#[repr(C)]
#[derive(Copy)]
pub struct Struct_eventhandler_entry_bpf_track {
    pub ee: Struct_eventhandler_entry,
    pub eh_func: bpf_track_fn,
}
impl ::core::clone::Clone for Struct_eventhandler_entry_bpf_track {
    fn clone(&self) -> Self { *self }
}
impl ::core::default::Default for Struct_eventhandler_entry_bpf_track {
    fn default() -> Self { unsafe { ::core::mem::zeroed() } }
}
pub enum Struct_image_params { }
pub type exitlist_fn =
    ::core::option::Option<extern "C" fn(arg1: *mut libc::c_void,
                                        arg2: *mut Struct_proc) -> ()>;
pub type forklist_fn =
    ::core::option::Option<extern "C" fn(arg1: *mut libc::c_void,
                                        arg2: *mut Struct_proc,
                                        arg3: *mut Struct_proc,
                                        arg4: libc::c_int) -> ()>;
pub type execlist_fn =
    ::core::option::Option<extern "C" fn(arg1: *mut libc::c_void,
                                        arg2: *mut Struct_proc,
                                        arg3: *mut Struct_image_params)
                              -> ()>;
pub type proc_ctor_fn =
    ::core::option::Option<extern "C" fn(arg1: *mut libc::c_void,
                                        arg2: *mut Struct_proc) -> ()>;
pub type proc_dtor_fn =
    ::core::option::Option<extern "C" fn(arg1: *mut libc::c_void,
                                        arg2: *mut Struct_proc) -> ()>;
pub type proc_init_fn =
    ::core::option::Option<extern "C" fn(arg1: *mut libc::c_void,
                                        arg2: *mut Struct_proc) -> ()>;
pub type proc_fini_fn =
    ::core::option::Option<extern "C" fn(arg1: *mut libc::c_void,
                                        arg2: *mut Struct_proc) -> ()>;
#[repr(C)]
#[derive(Copy)]
pub struct Struct_eventhandler_entry_process_ctor {
    pub ee: Struct_eventhandler_entry,
    pub eh_func: proc_ctor_fn,
}
impl ::core::clone::Clone for Struct_eventhandler_entry_process_ctor {
    fn clone(&self) -> Self { *self }
}
impl ::core::default::Default for Struct_eventhandler_entry_process_ctor {
    fn default() -> Self { unsafe { ::core::mem::zeroed() } }
}
#[repr(C)]
#[derive(Copy)]
pub struct Struct_eventhandler_entry_process_dtor {
    pub ee: Struct_eventhandler_entry,
    pub eh_func: proc_dtor_fn,
}
impl ::core::clone::Clone for Struct_eventhandler_entry_process_dtor {
    fn clone(&self) -> Self { *self }
}
impl ::core::default::Default for Struct_eventhandler_entry_process_dtor {
    fn default() -> Self { unsafe { ::core::mem::zeroed() } }
}
#[repr(C)]
#[derive(Copy)]
pub struct Struct_eventhandler_entry_process_init {
    pub ee: Struct_eventhandler_entry,
    pub eh_func: proc_init_fn,
}
impl ::core::clone::Clone for Struct_eventhandler_entry_process_init {
    fn clone(&self) -> Self { *self }
}
impl ::core::default::Default for Struct_eventhandler_entry_process_init {
    fn default() -> Self { unsafe { ::core::mem::zeroed() } }
}
#[repr(C)]
#[derive(Copy)]
pub struct Struct_eventhandler_entry_process_fini {
    pub ee: Struct_eventhandler_entry,
    pub eh_func: proc_fini_fn,
}
impl ::core::clone::Clone for Struct_eventhandler_entry_process_fini {
    fn clone(&self) -> Self { *self }
}
impl ::core::default::Default for Struct_eventhandler_entry_process_fini {
    fn default() -> Self { unsafe { ::core::mem::zeroed() } }
}
#[repr(C)]
#[derive(Copy)]
pub struct Struct_eventhandler_entry_process_exit {
    pub ee: Struct_eventhandler_entry,
    pub eh_func: exitlist_fn,
}
impl ::core::clone::Clone for Struct_eventhandler_entry_process_exit {
    fn clone(&self) -> Self { *self }
}
impl ::core::default::Default for Struct_eventhandler_entry_process_exit {
    fn default() -> Self { unsafe { ::core::mem::zeroed() } }
}
#[repr(C)]
#[derive(Copy)]
pub struct Struct_eventhandler_entry_process_fork {
    pub ee: Struct_eventhandler_entry,
    pub eh_func: forklist_fn,
}
impl ::core::clone::Clone for Struct_eventhandler_entry_process_fork {
    fn clone(&self) -> Self { *self }
}
impl ::core::default::Default for Struct_eventhandler_entry_process_fork {
    fn default() -> Self { unsafe { ::core::mem::zeroed() } }
}
#[repr(C)]
#[derive(Copy)]
pub struct Struct_eventhandler_entry_process_exec {
    pub ee: Struct_eventhandler_entry,
    pub eh_func: execlist_fn,
}
impl ::core::clone::Clone for Struct_eventhandler_entry_process_exec {
    fn clone(&self) -> Self { *self }
}
impl ::core::default::Default for Struct_eventhandler_entry_process_exec {
    fn default() -> Self { unsafe { ::core::mem::zeroed() } }
}
pub type app_coredump_start_fn =
    ::core::option::Option<extern "C" fn(arg1: *mut libc::c_void,
                                        arg2: *mut Struct_thread,
                                        name: *mut libc::c_char) -> ()>;
pub type app_coredump_progress_fn =
    ::core::option::Option<extern "C" fn(arg1: *mut libc::c_void,
                                        td: *mut Struct_thread,
                                        byte_count: libc::c_int) -> ()>;
pub type app_coredump_finish_fn =
    ::core::option::Option<extern "C" fn(arg1: *mut libc::c_void,
                                        td: *mut Struct_thread) -> ()>;
pub type app_coredump_error_fn =
    ::core::option::Option<extern "C" fn(arg1: *mut libc::c_void,
                                        td: *mut Struct_thread,
                                        msg: *mut libc::c_char, ...) -> ()>;
#[repr(C)]
#[derive(Copy)]
pub struct Struct_eventhandler_entry_app_coredump_start {
    pub ee: Struct_eventhandler_entry,
    pub eh_func: app_coredump_start_fn,
}
impl ::core::clone::Clone for Struct_eventhandler_entry_app_coredump_start {
    fn clone(&self) -> Self { *self }
}
impl ::core::default::Default for Struct_eventhandler_entry_app_coredump_start
 {
    fn default() -> Self { unsafe { ::core::mem::zeroed() } }
}
#[repr(C)]
#[derive(Copy)]
pub struct Struct_eventhandler_entry_app_coredump_progress {
    pub ee: Struct_eventhandler_entry,
    pub eh_func: app_coredump_progress_fn,
}
impl ::core::clone::Clone for Struct_eventhandler_entry_app_coredump_progress {
    fn clone(&self) -> Self { *self }
}
impl ::core::default::Default for
 Struct_eventhandler_entry_app_coredump_progress {
    fn default() -> Self { unsafe { ::core::mem::zeroed() } }
}
#[repr(C)]
#[derive(Copy)]
pub struct Struct_eventhandler_entry_app_coredump_finish {
    pub ee: Struct_eventhandler_entry,
    pub eh_func: app_coredump_finish_fn,
}
impl ::core::clone::Clone for Struct_eventhandler_entry_app_coredump_finish {
    fn clone(&self) -> Self { *self }
}
impl ::core::default::Default for Struct_eventhandler_entry_app_coredump_finish
 {
    fn default() -> Self { unsafe { ::core::mem::zeroed() } }
}
#[repr(C)]
#[derive(Copy)]
pub struct Struct_eventhandler_entry_app_coredump_error {
    pub ee: Struct_eventhandler_entry,
    pub eh_func: app_coredump_error_fn,
}
impl ::core::clone::Clone for Struct_eventhandler_entry_app_coredump_error {
    fn clone(&self) -> Self { *self }
}
impl ::core::default::Default for Struct_eventhandler_entry_app_coredump_error
 {
    fn default() -> Self { unsafe { ::core::mem::zeroed() } }
}
pub type thread_ctor_fn =
    ::core::option::Option<extern "C" fn(arg1: *mut libc::c_void,
                                        arg2: *mut Struct_thread) -> ()>;
pub type thread_dtor_fn =
    ::core::option::Option<extern "C" fn(arg1: *mut libc::c_void,
                                        arg2: *mut Struct_thread) -> ()>;
pub type thread_fini_fn =
    ::core::option::Option<extern "C" fn(arg1: *mut libc::c_void,
                                        arg2: *mut Struct_thread) -> ()>;
pub type thread_init_fn =
    ::core::option::Option<extern "C" fn(arg1: *mut libc::c_void,
                                        arg2: *mut Struct_thread) -> ()>;
#[repr(C)]
#[derive(Copy)]
pub struct Struct_eventhandler_entry_thread_ctor {
    pub ee: Struct_eventhandler_entry,
    pub eh_func: thread_ctor_fn,
}
impl ::core::clone::Clone for Struct_eventhandler_entry_thread_ctor {
    fn clone(&self) -> Self { *self }
}
impl ::core::default::Default for Struct_eventhandler_entry_thread_ctor {
    fn default() -> Self { unsafe { ::core::mem::zeroed() } }
}
#[repr(C)]
#[derive(Copy)]
pub struct Struct_eventhandler_entry_thread_dtor {
    pub ee: Struct_eventhandler_entry,
    pub eh_func: thread_dtor_fn,
}
impl ::core::clone::Clone for Struct_eventhandler_entry_thread_dtor {
    fn clone(&self) -> Self { *self }
}
impl ::core::default::Default for Struct_eventhandler_entry_thread_dtor {
    fn default() -> Self { unsafe { ::core::mem::zeroed() } }
}
#[repr(C)]
#[derive(Copy)]
pub struct Struct_eventhandler_entry_thread_init {
    pub ee: Struct_eventhandler_entry,
    pub eh_func: thread_init_fn,
}
impl ::core::clone::Clone for Struct_eventhandler_entry_thread_init {
    fn clone(&self) -> Self { *self }
}
impl ::core::default::Default for Struct_eventhandler_entry_thread_init {
    fn default() -> Self { unsafe { ::core::mem::zeroed() } }
}
#[repr(C)]
#[derive(Copy)]
pub struct Struct_eventhandler_entry_thread_fini {
    pub ee: Struct_eventhandler_entry,
    pub eh_func: thread_fini_fn,
}
impl ::core::clone::Clone for Struct_eventhandler_entry_thread_fini {
    fn clone(&self) -> Self { *self }
}
impl ::core::default::Default for Struct_eventhandler_entry_thread_fini {
    fn default() -> Self { unsafe { ::core::mem::zeroed() } }
}
pub type uma_zone_chfn =
    ::core::option::Option<extern "C" fn(arg1: *mut libc::c_void) -> ()>;
#[repr(C)]
#[derive(Copy)]
pub struct Struct_eventhandler_entry_nmbclusters_change {
    pub ee: Struct_eventhandler_entry,
    pub eh_func: uma_zone_chfn,
}
impl ::core::clone::Clone for Struct_eventhandler_entry_nmbclusters_change {
    fn clone(&self) -> Self { *self }
}
impl ::core::default::Default for Struct_eventhandler_entry_nmbclusters_change
 {
    fn default() -> Self { unsafe { ::core::mem::zeroed() } }
}
#[repr(C)]
#[derive(Copy)]
pub struct Struct_eventhandler_entry_nmbufs_change {
    pub ee: Struct_eventhandler_entry,
    pub eh_func: uma_zone_chfn,
}
impl ::core::clone::Clone for Struct_eventhandler_entry_nmbufs_change {
    fn clone(&self) -> Self { *self }
}
impl ::core::default::Default for Struct_eventhandler_entry_nmbufs_change {
    fn default() -> Self { unsafe { ::core::mem::zeroed() } }
}
#[repr(C)]
#[derive(Copy)]
pub struct Struct_eventhandler_entry_maxsockets_change {
    pub ee: Struct_eventhandler_entry,
    pub eh_func: uma_zone_chfn,
}
impl ::core::clone::Clone for Struct_eventhandler_entry_maxsockets_change {
    fn clone(&self) -> Self { *self }
}
impl ::core::default::Default for Struct_eventhandler_entry_maxsockets_change {
    fn default() -> Self { unsafe { ::core::mem::zeroed() } }
}
pub type kld_load_fn =
    ::core::option::Option<extern "C" fn(arg1: *mut libc::c_void,
                                        arg2: *mut Struct_linker_file) -> ()>;
pub type kld_unload_fn =
    ::core::option::Option<extern "C" fn(arg1: *mut libc::c_void,
                                        arg2: *const libc::c_char,
                                        arg3: caddr_t, arg4: size_t) -> ()>;
pub type kld_unload_try_fn =
    ::core::option::Option<extern "C" fn(arg1: *mut libc::c_void,
                                        arg2: *mut Struct_linker_file,
                                        arg3: *mut libc::c_int) -> ()>;
#[repr(C)]
#[derive(Copy)]
pub struct Struct_eventhandler_entry_kld_load {
    pub ee: Struct_eventhandler_entry,
    pub eh_func: kld_load_fn,
}
impl ::core::clone::Clone for Struct_eventhandler_entry_kld_load {
    fn clone(&self) -> Self { *self }
}
impl ::core::default::Default for Struct_eventhandler_entry_kld_load {
    fn default() -> Self { unsafe { ::core::mem::zeroed() } }
}
#[repr(C)]
#[derive(Copy)]
pub struct Struct_eventhandler_entry_kld_unload {
    pub ee: Struct_eventhandler_entry,
    pub eh_func: kld_unload_fn,
}
impl ::core::clone::Clone for Struct_eventhandler_entry_kld_unload {
    fn clone(&self) -> Self { *self }
}
impl ::core::default::Default for Struct_eventhandler_entry_kld_unload {
    fn default() -> Self { unsafe { ::core::mem::zeroed() } }
}
#[repr(C)]
#[derive(Copy)]
pub struct Struct_eventhandler_entry_kld_unload_try {
    pub ee: Struct_eventhandler_entry,
    pub eh_func: kld_unload_try_fn,
}
impl ::core::clone::Clone for Struct_eventhandler_entry_kld_unload_try {
    fn clone(&self) -> Self { *self }
}
impl ::core::default::Default for Struct_eventhandler_entry_kld_unload_try {
    fn default() -> Self { unsafe { ::core::mem::zeroed() } }
}
pub enum Struct_fb_info { }
pub type register_framebuffer_fn =
    ::core::option::Option<extern "C" fn(arg1: *mut libc::c_void,
                                        arg2: *mut Struct_fb_info) -> ()>;
pub type unregister_framebuffer_fn =
    ::core::option::Option<extern "C" fn(arg1: *mut libc::c_void,
                                        arg2: *mut Struct_fb_info) -> ()>;
#[repr(C)]
#[derive(Copy)]
pub struct Struct_eventhandler_entry_register_framebuffer {
    pub ee: Struct_eventhandler_entry,
    pub eh_func: register_framebuffer_fn,
}
impl ::core::clone::Clone for Struct_eventhandler_entry_register_framebuffer {
    fn clone(&self) -> Self { *self }
}
impl ::core::default::Default for
 Struct_eventhandler_entry_register_framebuffer {
    fn default() -> Self { unsafe { ::core::mem::zeroed() } }
}
#[repr(C)]
#[derive(Copy)]
pub struct Struct_eventhandler_entry_unregister_framebuffer {
    pub ee: Struct_eventhandler_entry,
    pub eh_func: unregister_framebuffer_fn,
}
impl ::core::clone::Clone for Struct_eventhandler_entry_unregister_framebuffer
 {
    fn clone(&self) -> Self { *self }
}
impl ::core::default::Default for
 Struct_eventhandler_entry_unregister_framebuffer {
    fn default() -> Self { unsafe { ::core::mem::zeroed() } }
}
pub enum Struct_snapdata { }
pub enum Struct_devfs_dirent { }
pub enum Struct_file { }
#[repr(C)]
#[derive(Copy)]
pub struct Struct_cdev {
    pub si_spare0: *mut libc::c_void,
    pub si_flags: u_int,
    pub si_atime: Struct_timespec,
    pub si_ctime: Struct_timespec,
    pub si_mtime: Struct_timespec,
    pub si_uid: uid_t,
    pub si_gid: gid_t,
    pub si_mode: mode_t,
    pub si_cred: *mut Struct_ucred,
    pub si_drv0: libc::c_int,
    pub si_refcount: libc::c_int,
    pub si_list: Struct_Unnamed14,
    pub si_clone: Struct_Unnamed15,
    pub si_children: Struct_Unnamed16,
    pub si_siblings: Struct_Unnamed17,
    pub si_parent: *mut Struct_cdev,
    pub si_mountpt: *mut Struct_mount,
    pub si_drv1: *mut libc::c_void,
    pub si_drv2: *mut libc::c_void,
    pub si_devsw: *mut Struct_cdevsw,
    pub si_iosize_max: libc::c_int,
    pub si_usecount: u_long,
    pub si_threadcount: u_long,
    pub __si_u: Union_Unnamed18,
    pub si_name: [libc::c_char; 64usize],
}
impl ::core::clone::Clone for Struct_cdev {
    fn clone(&self) -> Self { *self }
}
impl ::core::default::Default for Struct_cdev {
    fn default() -> Self { unsafe { ::core::mem::zeroed() } }
}
#[repr(C)]
#[derive(Copy)]
pub struct Struct_Unnamed14 {
    pub le_next: *mut Struct_cdev,
    pub le_prev: *mut *mut Struct_cdev,
}
impl ::core::clone::Clone for Struct_Unnamed14 {
    fn clone(&self) -> Self { *self }
}
impl ::core::default::Default for Struct_Unnamed14 {
    fn default() -> Self { unsafe { ::core::mem::zeroed() } }
}
#[repr(C)]
#[derive(Copy)]
pub struct Struct_Unnamed15 {
    pub le_next: *mut Struct_cdev,
    pub le_prev: *mut *mut Struct_cdev,
}
impl ::core::clone::Clone for Struct_Unnamed15 {
    fn clone(&self) -> Self { *self }
}
impl ::core::default::Default for Struct_Unnamed15 {
    fn default() -> Self { unsafe { ::core::mem::zeroed() } }
}
#[repr(C)]
#[derive(Copy)]
pub struct Struct_Unnamed16 {
    pub lh_first: *mut Struct_cdev,
}
impl ::core::clone::Clone for Struct_Unnamed16 {
    fn clone(&self) -> Self { *self }
}
impl ::core::default::Default for Struct_Unnamed16 {
    fn default() -> Self { unsafe { ::core::mem::zeroed() } }
}
#[repr(C)]
#[derive(Copy)]
pub struct Struct_Unnamed17 {
    pub le_next: *mut Struct_cdev,
    pub le_prev: *mut *mut Struct_cdev,
}
impl ::core::clone::Clone for Struct_Unnamed17 {
    fn clone(&self) -> Self { *self }
}
impl ::core::default::Default for Struct_Unnamed17 {
    fn default() -> Self { unsafe { ::core::mem::zeroed() } }
}
#[repr(C)]
#[derive(Copy)]
pub struct Union_Unnamed18 {
    pub _bindgen_data_: [u64; 1usize],
}
impl Union_Unnamed18 {
    pub unsafe fn __sid_snapdata(&mut self) -> *mut *mut Struct_snapdata {
        let raw: *mut u8 = ::core::mem::transmute(&self._bindgen_data_);
        ::core::mem::transmute(raw.offset(0))
    }
}
impl ::core::clone::Clone for Union_Unnamed18 {
    fn clone(&self) -> Self { *self }
}
impl ::core::default::Default for Union_Unnamed18 {
    fn default() -> Self { unsafe { ::core::mem::zeroed() } }
}
pub enum Struct_bio { }
pub enum Struct_buf { }
pub enum Struct_knote { }
pub enum Struct_clonedevs { }
pub enum Struct_vm_object { }
pub type d_thread_t = Struct_thread;
pub type d_open_t =
    extern "C" fn(dev: *mut Struct_cdev, oflags: libc::c_int,
                  devtype: libc::c_int, td: *mut Struct_thread)
        -> libc::c_int;
pub type d_fdopen_t =
    extern "C" fn(dev: *mut Struct_cdev, oflags: libc::c_int,
                  td: *mut Struct_thread, fp: *mut Struct_file)
        -> libc::c_int;
pub type d_close_t =
    extern "C" fn(dev: *mut Struct_cdev, fflag: libc::c_int,
                  devtype: libc::c_int, td: *mut Struct_thread)
        -> libc::c_int;
pub type d_strategy_t = extern "C" fn(bp: *mut Struct_bio) -> ();
pub type d_ioctl_t =
    extern "C" fn(dev: *mut Struct_cdev, cmd: u_long, data: caddr_t,
                  fflag: libc::c_int, td: *mut Struct_thread)
        -> libc::c_int;
pub type d_read_t =
    extern "C" fn(dev: *mut Struct_cdev, uio: *mut Struct_uio,
                  ioflag: libc::c_int) -> libc::c_int;
pub type d_write_t =
    extern "C" fn(dev: *mut Struct_cdev, uio: *mut Struct_uio,
                  ioflag: libc::c_int) -> libc::c_int;
pub type d_poll_t =
    extern "C" fn(dev: *mut Struct_cdev, events: libc::c_int,
                  td: *mut Struct_thread) -> libc::c_int;
pub type d_kqfilter_t =
    extern "C" fn(dev: *mut Struct_cdev, kn: *mut Struct_knote)
        -> libc::c_int;
pub type d_mmap_t =
    extern "C" fn(dev: *mut Struct_cdev, offset: vm_ooffset_t,
                  paddr: *mut vm_paddr_t, nprot: libc::c_int,
                  memattr: *mut vm_memattr_t) -> libc::c_int;
pub type d_mmap_single_t =
    extern "C" fn(cdev: *mut Struct_cdev, offset: *mut vm_ooffset_t,
                  size: vm_size_t, object: *mut *mut Struct_vm_object,
                  nprot: libc::c_int) -> libc::c_int;
pub type d_purge_t = extern "C" fn(dev: *mut Struct_cdev) -> ();
pub type dumper_t =
    extern "C" fn(_priv: *mut libc::c_void, _virtual: *mut libc::c_void,
                  _physical: vm_offset_t, _offset: off_t, _length: size_t)
        -> libc::c_int;
#[repr(C)]
#[derive(Copy)]
pub struct Struct_cdevsw {
    pub d_version: libc::c_int,
    pub d_flags: u_int,
    pub d_name: *const libc::c_char,
    pub d_open: *mut ::core::option::Option<extern "C" fn(Struct_cdev, i32, i32, Struct_thread) -> libc::c_int>,
    pub d_fdopen: *mut ::core::option::Option<extern "C" fn()
                                                 -> libc::c_int>,
    pub d_close: *mut ::core::option::Option<extern "C" fn(Struct_cdev, i32, i32, Struct_thread) -> libc::c_int>,
    pub d_read: *mut ::core::option::Option<extern "C" fn(Struct_cdev, Struct_uio, i32) -> libc::c_int>,
    pub d_write: *mut ::core::option::Option<extern "C" fn(Struct_cdev, Struct_uio, i32) -> libc::c_int>,
    pub d_ioctl: *mut ::core::option::Option<extern "C" fn() -> libc::c_int>,
    pub d_poll: *mut ::core::option::Option<extern "C" fn() -> libc::c_int>,
    pub d_mmap: *mut ::core::option::Option<extern "C" fn() -> libc::c_int>,
    pub d_strategy: *mut ::core::option::Option<extern "C" fn() -> ()>,
    pub d_dump: *mut ::core::option::Option<extern "C" fn() -> libc::c_int>,
    pub d_kqfilter: *mut ::core::option::Option<extern "C" fn()
                                                   -> libc::c_int>,
    pub d_purge: *mut ::core::option::Option<extern "C" fn() -> ()>,
    pub d_mmap_single: *mut ::core::option::Option<extern "C" fn()
                                                      -> libc::c_int>,
    pub d_spare0: [int32_t; 3usize],
    pub d_spare1: [*mut libc::c_void; 3usize],
    pub d_devs: Struct_Unnamed19,
    pub d_spare2: libc::c_int,
    pub __d_giant: Union_Unnamed20,
}
impl ::core::clone::Clone for Struct_cdevsw {
    fn clone(&self) -> Self { *self }
}
impl ::core::default::Default for Struct_cdevsw {
    fn default() -> Self { unsafe { ::core::mem::zeroed() } }
}
#[repr(C)]
#[derive(Copy)]
pub struct Struct_Unnamed19 {
    pub lh_first: *mut Struct_cdev,
}
impl ::core::clone::Clone for Struct_Unnamed19 {
    fn clone(&self) -> Self { *self }
}
impl ::core::default::Default for Struct_Unnamed19 {
    fn default() -> Self { unsafe { ::core::mem::zeroed() } }
}
#[repr(C)]
#[derive(Copy)]
pub struct Union_Unnamed20 {
    pub _bindgen_data_: [u64; 1usize],
}
impl Union_Unnamed20 {
    pub unsafe fn gianttrick(&mut self) -> *mut *mut Struct_cdevsw {
        let raw: *mut u8 = ::core::mem::transmute(&self._bindgen_data_);
        ::core::mem::transmute(raw.offset(0))
    }
    pub unsafe fn postfree_list(&mut self) -> *mut Struct_Unnamed21 {
        let raw: *mut u8 = ::core::mem::transmute(&self._bindgen_data_);
        ::core::mem::transmute(raw.offset(0))
    }
}
impl ::core::clone::Clone for Union_Unnamed20 {
    fn clone(&self) -> Self { *self }
}
impl ::core::default::Default for Union_Unnamed20 {
    fn default() -> Self { unsafe { ::core::mem::zeroed() } }
}
#[repr(C)]
#[derive(Copy)]
pub struct Struct_Unnamed21 {
    pub sle_next: *mut Struct_cdevsw,
}
impl ::core::clone::Clone for Struct_Unnamed21 {
    fn clone(&self) -> Self { *self }
}
impl ::core::default::Default for Struct_Unnamed21 {
    fn default() -> Self { unsafe { ::core::mem::zeroed() } }
}
#[repr(C)]
#[derive(Copy)]
pub struct Struct_devsw_module_data {
    pub chainevh: ::core::option::Option<extern "C" fn(arg1:
                                                          *mut Struct_module,
                                                      arg2: libc::c_int,
                                                      arg3:
                                                          *mut libc::c_void)
                                            -> libc::c_int>,
    pub chainarg: *mut libc::c_void,
}
impl ::core::clone::Clone for Struct_devsw_module_data {
    fn clone(&self) -> Self { *self }
}
impl ::core::default::Default for Struct_devsw_module_data {
    fn default() -> Self { unsafe { ::core::mem::zeroed() } }
}
pub type cdevpriv_dtr_t =
    ::core::option::Option<extern "C" fn(data: *mut libc::c_void) -> ()>;
pub type dev_clone_fn =
    ::core::option::Option<extern "C" fn(arg: *mut libc::c_void,
                                        cred: *mut Struct_ucred,
                                        name: *mut libc::c_char,
                                        namelen: libc::c_int,
                                        result: *mut *mut Struct_cdev) -> ()>;
#[repr(C)]
#[derive(Copy)]
pub struct Struct_eventhandler_entry_dev_clone {
    pub ee: Struct_eventhandler_entry,
    pub eh_func: dev_clone_fn,
}
impl ::core::clone::Clone for Struct_eventhandler_entry_dev_clone {
    fn clone(&self) -> Self { *self }
}
impl ::core::default::Default for Struct_eventhandler_entry_dev_clone {
    fn default() -> Self { unsafe { ::core::mem::zeroed() } }
}
#[repr(C)]
#[derive(Copy)]
pub struct Struct_dumperinfo {
    pub dumper: *mut ::core::option::Option<extern "C" fn() -> libc::c_int>,
    pub _priv: *mut libc::c_void,
    pub blocksize: u_int,
    pub maxiosize: u_int,
    pub mediaoffset: off_t,
    pub mediasize: off_t,
}
impl ::core::clone::Clone for Struct_dumperinfo {
    fn clone(&self) -> Self { *self }
}
impl ::core::default::Default for Struct_dumperinfo {
    fn default() -> Self { unsafe { ::core::mem::zeroed() } }
}
#[repr(C)]
#[derive(Copy)]
pub struct Struct_iovec {
    pub iov_base: *mut libc::c_void,
    pub iov_len: size_t,
}
impl ::core::clone::Clone for Struct_iovec {
    fn clone(&self) -> Self { *self }
}
impl ::core::default::Default for Struct_iovec {
    fn default() -> Self { unsafe { ::core::mem::zeroed() } }
}
pub type Enum_uio_rw = libc::c_uint;
pub const UIO_READ: libc::c_uint = 0;
pub const UIO_WRITE: libc::c_uint = 1;
pub type Enum_uio_seg = libc::c_uint;
pub const UIO_USERSPACE: libc::c_uint = 0;
pub const UIO_SYSSPACE: libc::c_uint = 1;
pub const UIO_NOCOPY: libc::c_uint = 2;
#[repr(C)]
#[derive(Copy)]
pub struct Struct_uio {
    pub uio_iov: *mut Struct_iovec,
    pub uio_iovcnt: libc::c_int,
    pub uio_offset: off_t,
    pub uio_resid: ssize_t,
    pub uio_segflg: Enum_uio_seg,
    pub uio_rw: Enum_uio_rw,
    pub uio_td: *mut Struct_thread,
}
impl ::core::clone::Clone for Struct_uio {
    fn clone(&self) -> Self { *self }
}
impl ::core::default::Default for Struct_uio {
    fn default() -> Self { unsafe { ::core::mem::zeroed() } }
}
#[repr(C)]
#[derive(Copy)]
pub struct Struct_malloc_type_stats {
    pub mts_memalloced: uint64_t,
    pub mts_memfreed: uint64_t,
    pub mts_numallocs: uint64_t,
    pub mts_numfrees: uint64_t,
    pub mts_size: uint64_t,
    pub _mts_reserved1: uint64_t,
    pub _mts_reserved2: uint64_t,
    pub _mts_reserved3: uint64_t,
}
impl ::core::clone::Clone for Struct_malloc_type_stats {
    fn clone(&self) -> Self { *self }
}
impl ::core::default::Default for Struct_malloc_type_stats {
    fn default() -> Self { unsafe { ::core::mem::zeroed() } }
}
#[repr(C)]
#[derive(Copy)]
pub struct Struct_malloc_type_internal {
    pub mti_probes: [uint32_t; 2usize],
    pub mti_zone: u_char,
    pub mti_stats: [Struct_malloc_type_stats; 1usize],
}
impl ::core::clone::Clone for Struct_malloc_type_internal {
    fn clone(&self) -> Self { *self }
}
impl ::core::default::Default for Struct_malloc_type_internal {
    fn default() -> Self { unsafe { ::core::mem::zeroed() } }
}
#[repr(C)]
#[derive(Copy)]
pub struct Struct_malloc_type {
    pub ks_next: *mut Struct_malloc_type,
    pub ks_magic: u_long,
    pub ks_shortdesc: *const libc::c_char,
    pub ks_handle: *mut libc::c_void,
}
impl ::core::clone::Clone for Struct_malloc_type {
    fn clone(&self) -> Self { *self }
}
impl ::core::default::Default for Struct_malloc_type {
    fn default() -> Self { unsafe { ::core::mem::zeroed() } }
}
#[repr(C)]
#[derive(Copy)]
pub struct Struct_malloc_type_stream_header {
    pub mtsh_version: uint32_t,
    pub mtsh_maxcpus: uint32_t,
    pub mtsh_count: uint32_t,
    pub _mtsh_pad: uint32_t,
}
impl ::core::clone::Clone for Struct_malloc_type_stream_header {
    fn clone(&self) -> Self { *self }
}
impl ::core::default::Default for Struct_malloc_type_stream_header {
    fn default() -> Self { unsafe { ::core::mem::zeroed() } }
}
#[repr(C)]
#[derive(Copy)]
pub struct Struct_malloc_type_header {
    pub mth_name: [libc::c_char; 32usize],
}
impl ::core::clone::Clone for Struct_malloc_type_header {
    fn clone(&self) -> Self { *self }
}
impl ::core::default::Default for Struct_malloc_type_header {
    fn default() -> Self { unsafe { ::core::mem::zeroed() } }
}
pub type malloc_type_list_func_t =
    extern "C" fn(arg1: *mut Struct_malloc_type, arg2: *mut libc::c_void)
        -> ();
pub type __va_list_tag = Struct___va_list_tag;
#[repr(C)]
#[derive(Copy)]
pub struct Struct___va_list_tag {
    pub gp_offset: libc::c_uint,
    pub fp_offset: libc::c_uint,
    pub overflow_arg_area: *mut libc::c_void,
    pub reg_save_area: *mut libc::c_void,
}
impl ::core::clone::Clone for Struct___va_list_tag {
    fn clone(&self) -> Self { *self }
}
impl ::core::default::Default for Struct___va_list_tag {
    fn default() -> Self { unsafe { ::core::mem::zeroed() } }
}
extern "C" {
    pub static mut modules_sx: Struct_sx;
    pub static mut cold: libc::c_int;
    pub static mut suspend_blocked: libc::c_int;
    pub static mut rebooting: libc::c_int;
    pub static mut panicstr: *const libc::c_char;
    pub static mut version: *mut libc::c_char;
    pub static mut compiler_version: *mut libc::c_char;
    pub static mut copyright: *mut libc::c_char;
    pub static mut kstack_pages: libc::c_int;
    pub static mut pagesizes: *mut u_long;
    pub static mut physmem: libc::c_long;
    pub static mut realmem: libc::c_long;
    pub static mut rootdevnames: [*mut libc::c_char; 2usize];
    pub static mut boothowto: libc::c_int;
    pub static mut bootverbose: libc::c_int;
    pub static mut maxusers: libc::c_int;
    pub static mut ngroups_max: libc::c_int;
    pub static mut vm_guest: libc::c_int;
    pub static mut osreldate: libc::c_int;
    pub static mut envmode: libc::c_int;
    pub static mut hintmode: libc::c_int;
    pub static mut dynamic_kenv: libc::c_int;
    pub static mut kenv_lock: Struct_mtx;
    pub static mut kern_envp: *mut libc::c_char;
    pub static mut static_env: *mut libc::c_char;
    pub static mut static_hints: *mut libc::c_char;
    pub static mut kenvp: *mut *mut libc::c_char;
    pub static mut zero_region: *const libc::c_void;
    pub static mut unmapped_buf_allowed: libc::c_int;
    pub static mut iosize_max_clamp: libc::c_int;
    pub static mut devfs_iosize_max_clamp: libc::c_int;
    pub static mut cpu_deepest_sleep: libc::c_int;
    pub static mut cpu_disable_c2_sleep: libc::c_int;
    pub static mut cpu_disable_c3_sleep: libc::c_int;
    pub static mut cpu_ticks: *mut cpu_tick_f;
    pub static mut bcd2bin_data: *const u_char;
    pub static mut bin2bcd_data: *const u_char;
    pub static mut hex2ascii_data: *const libc::c_char;
    pub static mut arc4rand_iniseed_state: libc::c_int;
    pub static mut crc32_tab: *const uint32_t;
    pub static mut softdep_ast_cleanup:
               ::core::option::Option<extern "C" fn() -> ()>;
    pub static mut time_second: time_t;
    pub static mut time_uptime: time_t;
    pub static mut boottimebin: Struct_bintime;
    pub static mut boottime: Struct_timeval;
    pub static mut tc_tick_bt: Struct_bintime;
    pub static mut tc_tick_sbt: sbintime_t;
    pub static mut tick_bt: Struct_bintime;
    pub static mut tick_sbt: sbintime_t;
    pub static mut tc_precexp: libc::c_int;
    pub static mut tc_timepercentage: libc::c_int;
    pub static mut bt_timethreshold: Struct_bintime;
    pub static mut bt_tickthreshold: Struct_bintime;
    pub static mut sbt_timethreshold: sbintime_t;
    pub static mut sbt_tickthreshold: sbintime_t;
    pub static mut kernelname: [libc::c_char; 1024usize];
    pub static mut tick: libc::c_int;
    pub static mut hz: libc::c_int;
    pub static mut psratio: libc::c_int;
    pub static mut stathz: libc::c_int;
    pub static mut profhz: libc::c_int;
    pub static mut profprocs: libc::c_int;
    pub static mut ticks: libc::c_int;
    pub static mut lock_class_mtx_sleep: Struct_lock_class;
    pub static mut lock_class_mtx_spin: Struct_lock_class;
    pub static mut lock_class_sx: Struct_lock_class;
    pub static mut lock_class_rw: Struct_lock_class;
    pub static mut lock_class_rm: Struct_lock_class;
    pub static mut lock_class_rm_sleepable: Struct_lock_class;
    pub static mut lock_class_lockmgr: Struct_lock_class;
    pub static mut lock_classes: *mut *mut Struct_lock_class;
    pub static mut ktr_cpumask: cpuset_t;
    pub static mut ktr_mask: libc::c_int;
    pub static mut ktr_entries: libc::c_int;
    pub static mut ktr_verbose: libc::c_int;
    pub static mut ktr_idx: libc::c_int;
    pub static mut ktr_buf: *mut Struct_ktr_entry;
    pub static mut cnt: Struct_vmmeter;
    pub static mut vm_pageout_wakeup_thresh: libc::c_int;
    pub static mut averunnable: Struct_loadavg;
    pub static mut __start_set_pcpu: *mut uintptr_t;
    pub static mut __stop_set_pcpu: *mut uintptr_t;
    pub static mut dpcpu_off: *mut uintptr_t;
    pub static mut cpuhead: Struct_cpuhead;
    pub static mut cpuid_to_pcpu: *mut *mut Struct_pcpu;
    pub static mut lockstat_probemap: [uint32_t; 29usize];
    pub static mut lockstat_probe_func: lockstat_probe_func_t;
    pub static mut lockstat_enabled: libc::c_int;
    pub static mut mtxpool_lockbuilder: *mut Struct_mtx_pool;
    pub static mut mtxpool_sleep: *mut Struct_mtx_pool;
    pub static mut Giant: Struct_mtx;
    pub static mut blocked_lock: Struct_mtx;
    pub static mut dumping: libc::c_int;
    pub static mut M_CACHE: [Struct_malloc_type; 1usize];
    pub static mut M_DEVBUF: [Struct_malloc_type; 1usize];
    pub static mut M_TEMP: [Struct_malloc_type; 1usize];
    pub static mut M_IP6OPT: [Struct_malloc_type; 1usize];
    pub static mut M_IP6NDP: [Struct_malloc_type; 1usize];
    pub static mut M_IOV: [Struct_malloc_type; 1usize];
    pub static mut malloc_mtx: Struct_mtx;
}
extern "C" {
    pub fn module_register_init(arg1: *const libc::c_void) -> ();
    pub fn module_register(arg1: *const Struct_moduledata,
                           arg2: *mut Struct_linker_file) -> libc::c_int;
    pub fn module_lookupbyname(arg1: *const libc::c_char) -> module_t;
    pub fn module_lookupbyid(arg1: libc::c_int) -> module_t;
    pub fn module_quiesce(arg1: module_t) -> libc::c_int;
    pub fn module_reference(arg1: module_t) -> ();
    pub fn module_release(arg1: module_t) -> ();
    pub fn module_unload(arg1: module_t) -> libc::c_int;
    pub fn module_getid(arg1: module_t) -> libc::c_int;
    pub fn module_getfnext(arg1: module_t) -> module_t;
    pub fn module_getname(arg1: module_t) -> *const libc::c_char;
    pub fn module_setspecific(arg1: module_t, arg2: *mut modspecific_t) -> ();
    pub fn module_file(arg1: module_t) -> *mut Struct_linker_file;
    pub fn reset_dbregs() -> ();
    pub fn rdmsr_safe(msr: u_int, val: *mut uint64_t) -> libc::c_int;
    pub fn wrmsr_safe(msr: u_int, newval: uint64_t) -> libc::c_int;
    pub fn callout_init(arg1: *mut Struct_callout, arg2: libc::c_int) -> ();
    pub fn _callout_init_lock(arg1: *mut Struct_callout,
                              arg2: *mut Struct_lock_object,
                              arg3: libc::c_int) -> ();
    pub fn callout_reset_sbt_on(arg1: *mut Struct_callout, arg2: sbintime_t,
                                arg3: sbintime_t,
                                arg4:
                                    ::core::option::Option<extern "C" fn(arg1:
                                                                            *mut libc::c_void)
                                                              -> ()>,
                                arg5: *mut libc::c_void,
                                arg6: libc::c_int, arg7: libc::c_int)
     -> libc::c_int;
    pub fn callout_schedule(arg1: *mut Struct_callout, arg2: libc::c_int)
     -> libc::c_int;
    pub fn callout_schedule_on(arg1: *mut Struct_callout, arg2: libc::c_int,
                               arg3: libc::c_int) -> libc::c_int;
    pub fn _callout_stop_safe(arg1: *mut Struct_callout, arg2: libc::c_int)
     -> libc::c_int;
    pub fn callout_process(now: sbintime_t) -> ();
    pub fn setjmp(arg1: *mut Struct__jmp_buf) -> libc::c_int;
    pub fn longjmp(arg1: *mut Struct__jmp_buf, arg2: libc::c_int) -> ();
    pub fn dumpstatus(addr: vm_offset_t, count: off_t) -> libc::c_int;
    pub fn nullop() -> libc::c_int;
    pub fn eopnotsupp() -> libc::c_int;
    pub fn ureadc(arg1: libc::c_int, arg2: *mut Struct_uio)
     -> libc::c_int;
    pub fn hashdestroy(arg1: *mut libc::c_void,
                       arg2: *mut Struct_malloc_type, arg3: u_long) -> ();
    pub fn hashinit(count: libc::c_int, _type: *mut Struct_malloc_type,
                    hashmask: *mut u_long) -> *mut libc::c_void;
    pub fn hashinit_flags(count: libc::c_int,
                          _type: *mut Struct_malloc_type,
                          hashmask: *mut u_long, flags: libc::c_int)
     -> *mut libc::c_void;
    pub fn phashinit(count: libc::c_int, _type: *mut Struct_malloc_type,
                     nentries: *mut u_long) -> *mut libc::c_void;
    pub fn g_waitidle() -> ();
    pub fn panic(arg1: *const libc::c_char, ...) -> ();
    pub fn vpanic(arg1: *const libc::c_char, arg2: __va_list) -> ();
    pub fn cpu_boot(arg1: libc::c_int) -> ();
    pub fn cpu_flush_dcache(arg1: *mut libc::c_void, arg2: size_t) -> ();
    pub fn cpu_rootconf() -> ();
    pub fn critical_enter() -> ();
    pub fn critical_exit() -> ();
    pub fn init_param1() -> ();
    pub fn init_param2(physpages: libc::c_long) -> ();
    pub fn init_static_kenv(arg1: *mut libc::c_char, arg2: size_t) -> ();
    pub fn tablefull(arg1: *const libc::c_char) -> ();
    pub fn kvprintf(arg1: *const libc::c_char,
                    arg2:
                        ::core::option::Option<extern "C" fn(arg1:
                                                                libc::c_int,
                                                            arg2:
                                                                *mut libc::c_void)
                                                  -> ()>,
                    arg3: *mut libc::c_void, arg4: libc::c_int,
                    arg5: __va_list) -> libc::c_int;
    pub fn log(arg1: libc::c_int, arg2: *const libc::c_char, ...) -> ();
    pub fn log_console(arg1: *mut Struct_uio) -> ();
    pub fn asprintf(ret: *mut *mut libc::c_char,
                    mtp: *mut Struct_malloc_type,
                    format: *const libc::c_char, ...) -> libc::c_int;
    pub fn printf(arg1: *const libc::c_char, ...) -> libc::c_int;
    pub fn snprintf(arg1: *mut libc::c_char, arg2: size_t,
                    arg3: *const libc::c_char, ...) -> libc::c_int;
    pub fn sprintf(buf: *mut libc::c_char, arg1: *const libc::c_char, ...)
     -> libc::c_int;
    pub fn uprintf(arg1: *const libc::c_char, ...) -> libc::c_int;
    pub fn vprintf(arg1: *const libc::c_char, arg2: __va_list)
     -> libc::c_int;
    pub fn vasprintf(ret: *mut *mut libc::c_char,
                     mtp: *mut Struct_malloc_type,
                     format: *const libc::c_char, ap: __va_list)
     -> libc::c_int;
    pub fn vsnprintf(arg1: *mut libc::c_char, arg2: size_t,
                     arg3: *const libc::c_char, arg4: __va_list)
     -> libc::c_int;
    pub fn vsnrprintf(arg1: *mut libc::c_char, arg2: size_t,
                      arg3: libc::c_int, arg4: *const libc::c_char,
                      arg5: __va_list) -> libc::c_int;
    pub fn vsprintf(buf: *mut libc::c_char, arg1: *const libc::c_char,
                    arg2: __va_list) -> libc::c_int;
    pub fn ttyprintf(arg1: *mut Struct_tty, arg2: *const libc::c_char, ...)
     -> libc::c_int;
    pub fn sscanf(arg1: *const libc::c_char,
                  arg2: *const libc::c_char, ...) -> libc::c_int;
    pub fn vsscanf(arg1: *const libc::c_char, arg2: *const libc::c_char,
                   arg3: __va_list) -> libc::c_int;
    pub fn strtol(arg1: *const libc::c_char, arg2: *mut *mut libc::c_char,
                  arg3: libc::c_int) -> libc::c_long;
    pub fn strtoul(arg1: *const libc::c_char,
                   arg2: *mut *mut libc::c_char, arg3: libc::c_int)
     -> u_long;
    pub fn strtoq(arg1: *const libc::c_char, arg2: *mut *mut libc::c_char,
                  arg3: libc::c_int) -> quad_t;
    pub fn strtouq(arg1: *const libc::c_char,
                   arg2: *mut *mut libc::c_char, arg3: libc::c_int)
     -> u_quad_t;
    pub fn tprintf(p: *mut Struct_proc, pri: libc::c_int,
                   arg1: *const libc::c_char, ...) -> ();
    pub fn vtprintf(arg1: *mut Struct_proc, arg2: libc::c_int,
                    arg3: *const libc::c_char, arg4: __va_list) -> ();
    pub fn hexdump(ptr: *const libc::c_void, length: libc::c_int,
                   hdr: *const libc::c_char, flags: libc::c_int) -> ();
    pub fn bcopy(from: *const libc::c_void, to: *mut libc::c_void,
                 len: size_t) -> ();
    pub fn bzero(buf: *mut libc::c_void, len: size_t) -> ();
    pub fn memcpy(to: *mut libc::c_void, from: *const libc::c_void,
                  len: size_t) -> *mut libc::c_void;
    pub fn memmove(dest: *mut libc::c_void, src: *const libc::c_void,
                   n: size_t) -> *mut libc::c_void;
    pub fn copystr(kfaddr: *const libc::c_void, kdaddr: *mut libc::c_void,
                   len: size_t, lencopied: *mut size_t) -> libc::c_int;
    pub fn copyinstr(udaddr: *const libc::c_void,
                     kaddr: *mut libc::c_void, len: size_t,
                     lencopied: *mut size_t) -> libc::c_int;
    pub fn copyin(udaddr: *const libc::c_void, kaddr: *mut libc::c_void,
                  len: size_t) -> libc::c_int;
    pub fn copyin_nofault(udaddr: *const libc::c_void,
                          kaddr: *mut libc::c_void, len: size_t)
     -> libc::c_int;
    pub fn copyout(kaddr: *const libc::c_void, udaddr: *mut libc::c_void,
                   len: size_t) -> libc::c_int;
    pub fn copyout_nofault(kaddr: *const libc::c_void,
                           udaddr: *mut libc::c_void, len: size_t)
     -> libc::c_int;
    pub fn fubyte(base: *const libc::c_void) -> libc::c_int;
    pub fn fuword(base: *const libc::c_void) -> libc::c_long;
    pub fn fuword16(base: *const libc::c_void) -> libc::c_int;
    pub fn fuword32(base: *const libc::c_void) -> int32_t;
    pub fn fuword64(base: *const libc::c_void) -> int64_t;
    pub fn fueword(base: *const libc::c_void, val: *mut libc::c_long)
     -> libc::c_int;
    pub fn fueword32(base: *const libc::c_void, val: *mut int32_t)
     -> libc::c_int;
    pub fn fueword64(base: *const libc::c_void, val: *mut int64_t)
     -> libc::c_int;
    pub fn subyte(base: *mut libc::c_void, byte: libc::c_int)
     -> libc::c_int;
    pub fn suword(base: *mut libc::c_void, word: libc::c_long)
     -> libc::c_int;
    pub fn suword16(base: *mut libc::c_void, word: libc::c_int)
     -> libc::c_int;
    pub fn suword32(base: *mut libc::c_void, word: int32_t)
     -> libc::c_int;
    pub fn suword64(base: *mut libc::c_void, word: int64_t)
     -> libc::c_int;
    pub fn casuword32(base: *mut uint32_t, oldval: uint32_t, newval: uint32_t)
     -> uint32_t;
    pub fn casuword(p: *mut u_long, oldval: u_long, newval: u_long) -> u_long;
    pub fn casueword32(base: *mut uint32_t, oldval: uint32_t,
                       oldvalp: *mut uint32_t, newval: uint32_t)
     -> libc::c_int;
    pub fn casueword(p: *mut u_long, oldval: u_long, oldvalp: *mut u_long,
                     newval: u_long) -> libc::c_int;
    pub fn realitexpire(arg1: *mut libc::c_void) -> ();
    pub fn sysbeep(hertz: libc::c_int, period: libc::c_int)
     -> libc::c_int;
    pub fn hardclock(usermode: libc::c_int, pc: uintfptr_t) -> ();
    pub fn hardclock_cnt(cnt: libc::c_int, usermode: libc::c_int) -> ();
    pub fn hardclock_cpu(usermode: libc::c_int) -> ();
    pub fn hardclock_sync(cpu: libc::c_int) -> ();
    pub fn softclock(arg1: *mut libc::c_void) -> ();
    pub fn statclock(usermode: libc::c_int) -> ();
    pub fn statclock_cnt(cnt: libc::c_int, usermode: libc::c_int) -> ();
    pub fn profclock(usermode: libc::c_int, pc: uintfptr_t) -> ();
    pub fn profclock_cnt(cnt: libc::c_int, usermode: libc::c_int,
                         pc: uintfptr_t) -> ();
    pub fn hardclockintr() -> libc::c_int;
    pub fn startprofclock(arg1: *mut Struct_proc) -> ();
    pub fn stopprofclock(arg1: *mut Struct_proc) -> ();
    pub fn cpu_startprofclock() -> ();
    pub fn cpu_stopprofclock() -> ();
    pub fn cpu_idleclock() -> sbintime_t;
    pub fn cpu_activeclock() -> ();
    pub fn cpu_new_callout(cpu: libc::c_int, bt: sbintime_t,
                           bt_opt: sbintime_t) -> ();
    pub fn cpu_et_frequency(et: *mut Struct_eventtimer, newfreq: uint64_t)
     -> ();
    pub fn cr_cansee(u1: *mut Struct_ucred, u2: *mut Struct_ucred)
     -> libc::c_int;
    pub fn cr_canseesocket(cred: *mut Struct_ucred, so: *mut Struct_socket)
     -> libc::c_int;
    pub fn cr_canseeinpcb(cred: *mut Struct_ucred, inp: *mut Struct_inpcb)
     -> libc::c_int;
    pub fn getenv(name: *const libc::c_char) -> *mut libc::c_char;
    pub fn freeenv(env: *mut libc::c_char) -> ();
    pub fn getenv_int(name: *const libc::c_char, data: *mut libc::c_int)
     -> libc::c_int;
    pub fn getenv_uint(name: *const libc::c_char, data: *mut libc::c_uint)
     -> libc::c_int;
    pub fn getenv_long(name: *const libc::c_char, data: *mut libc::c_long)
     -> libc::c_int;
    pub fn getenv_ulong(name: *const libc::c_char,
                        data: *mut libc::c_ulong) -> libc::c_int;
    pub fn getenv_string(name: *const libc::c_char,
                         data: *mut libc::c_char, size: libc::c_int)
     -> libc::c_int;
    pub fn getenv_quad(name: *const libc::c_char, data: *mut quad_t)
     -> libc::c_int;
    pub fn setenv(name: *const libc::c_char, value: *const libc::c_char)
     -> libc::c_int;
    pub fn unsetenv(name: *const libc::c_char) -> libc::c_int;
    pub fn testenv(name: *const libc::c_char) -> libc::c_int;
    pub fn set_cputicker(func: *mut cpu_tick_f, freq: uint64_t,
                         var: libc::c_uint) -> ();
    pub fn cpu_tickrate() -> uint64_t;
    pub fn cputick2usec(tick: uint64_t) -> uint64_t;
    pub fn arc4random() -> uint32_t;
    pub fn arc4rand(ptr: *mut libc::c_void, len: u_int,
                    reseed: libc::c_int) -> ();
    pub fn bcmp(arg1: *const libc::c_void, arg2: *const libc::c_void,
                arg3: size_t) -> libc::c_int;
    pub fn bsearch(arg1: *const libc::c_void, arg2: *const libc::c_void,
                   arg3: size_t, arg4: size_t,
                   arg5:
                       ::core::option::Option<extern "C" fn(arg1:
                                                               *const libc::c_void,
                                                           arg2:
                                                               *const libc::c_void)
                                                 -> libc::c_int>)
     -> *mut libc::c_void;
    pub fn fnmatch(arg1: *const libc::c_char, arg2: *const libc::c_char,
                   arg3: libc::c_int) -> libc::c_int;
    pub fn locc(arg1: libc::c_int, arg2: *mut libc::c_char, arg3: u_int)
     -> libc::c_int;
    pub fn memchr(s: *const libc::c_void, c: libc::c_int, n: size_t)
     -> *mut libc::c_void;
    pub fn memcchr(s: *const libc::c_void, c: libc::c_int, n: size_t)
     -> *mut libc::c_void;
    pub fn memcmp(b1: *const libc::c_void, b2: *const libc::c_void,
                  len: size_t) -> libc::c_int;
    pub fn qsort(base: *mut libc::c_void, nmemb: size_t, size: size_t,
                 compar:
                     ::core::option::Option<extern "C" fn(arg1:
                                                             *const libc::c_void,
                                                         arg2:
                                                             *const libc::c_void)
                                               -> libc::c_int>) -> ();
    pub fn qsort_r(base: *mut libc::c_void, nmemb: size_t, size: size_t,
                   thunk: *mut libc::c_void,
                   compar:
                       ::core::option::Option<extern "C" fn(arg1:
                                                               *mut libc::c_void,
                                                           arg2:
                                                               *const libc::c_void,
                                                           arg3:
                                                               *const libc::c_void)
                                                 -> libc::c_int>) -> ();
    pub fn random() -> u_long;
    pub fn scanc(arg1: u_int, arg2: *const u_char, arg3: *const u_char,
                 arg4: libc::c_int) -> libc::c_int;
    pub fn srandom(arg1: u_long) -> ();
    pub fn strcasecmp(arg1: *const libc::c_char,
                      arg2: *const libc::c_char) -> libc::c_int;
    pub fn strcat(arg1: *mut libc::c_char, arg2: *const libc::c_char)
     -> *mut libc::c_char;
    pub fn strchr(arg1: *const libc::c_char, arg2: libc::c_int)
     -> *mut libc::c_char;
    pub fn strcmp(arg1: *const libc::c_char, arg2: *const libc::c_char)
     -> libc::c_int;
    pub fn strcpy(arg1: *mut libc::c_char, arg2: *const libc::c_char)
     -> *mut libc::c_char;
    pub fn strcspn(arg1: *const libc::c_char, arg2: *const libc::c_char)
     -> size_t;
    pub fn strdup(arg1: *const libc::c_char, arg2: *mut Struct_malloc_type)
     -> *mut libc::c_char;
    pub fn strndup(arg1: *const libc::c_char, arg2: size_t,
                   arg3: *mut Struct_malloc_type) -> *mut libc::c_char;
    pub fn strlcat(arg1: *mut libc::c_char, arg2: *const libc::c_char,
                   arg3: size_t) -> size_t;
    pub fn strlcpy(arg1: *mut libc::c_char, arg2: *const libc::c_char,
                   arg3: size_t) -> size_t;
    pub fn strlen(arg1: *const libc::c_char) -> size_t;
    pub fn strncasecmp(arg1: *const libc::c_char,
                       arg2: *const libc::c_char, arg3: size_t)
     -> libc::c_int;
    pub fn strncmp(arg1: *const libc::c_char, arg2: *const libc::c_char,
                   arg3: size_t) -> libc::c_int;
    pub fn strncpy(arg1: *mut libc::c_char, arg2: *const libc::c_char,
                   arg3: size_t) -> *mut libc::c_char;
    pub fn strnlen(arg1: *const libc::c_char, arg2: size_t) -> size_t;
    pub fn strrchr(arg1: *const libc::c_char, arg2: libc::c_int)
     -> *mut libc::c_char;
    pub fn strsep(arg1: *mut *mut libc::c_char,
                  delim: *const libc::c_char) -> *mut libc::c_char;
    pub fn strspn(arg1: *const libc::c_char, arg2: *const libc::c_char)
     -> size_t;
    pub fn strstr(arg1: *const libc::c_char, arg2: *const libc::c_char)
     -> *mut libc::c_char;
    pub fn strvalid(arg1: *const libc::c_char, arg2: size_t)
     -> libc::c_int;
    pub fn calculate_crc32c(crc32c: uint32_t, buffer: *const libc::c_uchar,
                            length: libc::c_uint) -> uint32_t;
    pub fn consinit() -> ();
    pub fn cpu_initclocks() -> ();
    pub fn cpu_initclocks_bsp() -> ();
    pub fn cpu_initclocks_ap() -> ();
    pub fn usrinfoinit() -> ();
    pub fn kern_reboot(arg1: libc::c_int) -> ();
    pub fn shutdown_nice(arg1: libc::c_int) -> ();
    pub fn callout_handle_init(arg1: *mut Struct_callout_handle) -> ();
    pub fn timeout(arg1: *mut ::core::option::Option<extern "C" fn() -> ()>,
                   arg2: *mut libc::c_void, arg3: libc::c_int)
     -> Struct_callout_handle;
    pub fn untimeout(arg1: *mut ::core::option::Option<extern "C" fn() -> ()>,
                     arg2: *mut libc::c_void, arg3: Struct_callout_handle)
     -> ();
    pub fn _sleep(chan: *mut libc::c_void, lock: *mut Struct_lock_object,
                  pri: libc::c_int, wmesg: *const libc::c_char,
                  sbt: sbintime_t, pr: sbintime_t, flags: libc::c_int)
     -> libc::c_int;
    pub fn msleep_spin_sbt(chan: *mut libc::c_void, mtx: *mut Struct_mtx,
                           wmesg: *const libc::c_char, sbt: sbintime_t,
                           pr: sbintime_t, flags: libc::c_int)
     -> libc::c_int;
    pub fn pause_sbt(wmesg: *const libc::c_char, sbt: sbintime_t,
                     pr: sbintime_t, flags: libc::c_int) -> libc::c_int;
    pub fn wakeup(chan: *mut libc::c_void) -> ();
    pub fn wakeup_one(chan: *mut libc::c_void) -> ();
    pub fn dev2udev(x: *mut Struct_cdev) -> dev_t;
    pub fn devtoname(cdev: *mut Struct_cdev) -> *const libc::c_char;
    pub fn poll_no_poll(events: libc::c_int) -> libc::c_int;
    pub fn DELAY(usec: libc::c_int) -> ();
    pub fn root_mount_hold(identifier: *const libc::c_char)
     -> *mut Struct_root_hold_token;
    pub fn root_mount_rel(h: *mut Struct_root_hold_token) -> ();
    pub fn root_mount_wait() -> ();
    pub fn root_mounted() -> libc::c_int;
    pub fn new_unrhdr(low: libc::c_int, high: libc::c_int,
                      mutex: *mut Struct_mtx) -> *mut Struct_unrhdr;
    pub fn init_unrhdr(uh: *mut Struct_unrhdr, low: libc::c_int,
                       high: libc::c_int, mutex: *mut Struct_mtx) -> ();
    pub fn delete_unrhdr(uh: *mut Struct_unrhdr) -> ();
    pub fn clean_unrhdr(uh: *mut Struct_unrhdr) -> ();
    pub fn clean_unrhdrl(uh: *mut Struct_unrhdr) -> ();
    pub fn alloc_unr(uh: *mut Struct_unrhdr) -> libc::c_int;
    pub fn alloc_unr_specific(uh: *mut Struct_unrhdr, item: u_int)
     -> libc::c_int;
    pub fn alloc_unrl(uh: *mut Struct_unrhdr) -> libc::c_int;
    pub fn free_unr(uh: *mut Struct_unrhdr, item: u_int) -> ();
    pub fn intr_prof_stack_use(td: *mut Struct_thread,
                               frame: *mut Struct_trapframe) -> ();
    pub fn inittodr(base: time_t) -> ();
    pub fn resettodr() -> ();
    pub fn binuptime(bt: *mut Struct_bintime) -> ();
    pub fn nanouptime(tsp: *mut Struct_timespec) -> ();
    pub fn microuptime(tvp: *mut Struct_timeval) -> ();
    pub fn bintime(bt: *mut Struct_bintime) -> ();
    pub fn nanotime(tsp: *mut Struct_timespec) -> ();
    pub fn microtime(tvp: *mut Struct_timeval) -> ();
    pub fn getbinuptime(bt: *mut Struct_bintime) -> ();
    pub fn getnanouptime(tsp: *mut Struct_timespec) -> ();
    pub fn getmicrouptime(tvp: *mut Struct_timeval) -> ();
    pub fn getbintime(bt: *mut Struct_bintime) -> ();
    pub fn getnanotime(tsp: *mut Struct_timespec) -> ();
    pub fn getmicrotime(tvp: *mut Struct_timeval) -> ();
    pub fn itimerdecr(itp: *mut Struct_itimerval, usec: libc::c_int)
     -> libc::c_int;
    pub fn itimerfix(tv: *mut Struct_timeval) -> libc::c_int;
    pub fn ppsratecheck(arg1: *mut Struct_timeval, arg2: *mut libc::c_int,
                        arg3: libc::c_int) -> libc::c_int;
    pub fn ratecheck(arg1: *mut Struct_timeval, arg2: *const Struct_timeval)
     -> libc::c_int;
    pub fn timevaladd(t1: *mut Struct_timeval, t2: *const Struct_timeval)
     -> ();
    pub fn timevalsub(t1: *mut Struct_timeval, t2: *const Struct_timeval)
     -> ();
    pub fn tvtohz(tv: *mut Struct_timeval) -> libc::c_int;
    pub fn htonl(arg1: __uint32_t) -> __uint32_t;
    pub fn htons(arg1: __uint16_t) -> __uint16_t;
    pub fn ntohl(arg1: __uint32_t) -> __uint32_t;
    pub fn ntohs(arg1: __uint16_t) -> __uint16_t;
    pub fn sysinit_add(set: *mut *mut Struct_sysinit,
                       set_end: *mut *mut Struct_sysinit) -> ();
    pub fn tunable_int_init(arg1: *mut libc::c_void) -> ();
    pub fn tunable_long_init(arg1: *mut libc::c_void) -> ();
    pub fn tunable_ulong_init(arg1: *mut libc::c_void) -> ();
    pub fn tunable_quad_init(arg1: *mut libc::c_void) -> ();
    pub fn tunable_str_init(arg1: *mut libc::c_void) -> ();
    pub fn config_intrhook_establish(hook: *mut Struct_intr_config_hook)
     -> libc::c_int;
    pub fn config_intrhook_disestablish(hook: *mut Struct_intr_config_hook)
     -> ();
    pub fn lock_init(arg1: *mut Struct_lock_object,
                     arg2: *mut Struct_lock_class,
                     arg3: *const libc::c_char, arg4: *const libc::c_char,
                     arg5: libc::c_int) -> ();
    pub fn lock_destroy(arg1: *mut Struct_lock_object) -> ();
    pub fn spinlock_enter() -> ();
    pub fn spinlock_exit() -> ();
    pub fn witness_init(arg1: *mut Struct_lock_object,
                        arg2: *const libc::c_char) -> ();
    pub fn witness_destroy(arg1: *mut Struct_lock_object) -> ();
    pub fn witness_defineorder(arg1: *mut Struct_lock_object,
                               arg2: *mut Struct_lock_object)
     -> libc::c_int;
    pub fn witness_checkorder(arg1: *mut Struct_lock_object,
                              arg2: libc::c_int,
                              arg3: *const libc::c_char,
                              arg4: libc::c_int,
                              arg5: *mut Struct_lock_object) -> ();
    pub fn witness_lock(arg1: *mut Struct_lock_object, arg2: libc::c_int,
                        arg3: *const libc::c_char, arg4: libc::c_int)
     -> ();
    pub fn witness_upgrade(arg1: *mut Struct_lock_object, arg2: libc::c_int,
                           arg3: *const libc::c_char, arg4: libc::c_int)
     -> ();
    pub fn witness_downgrade(arg1: *mut Struct_lock_object,
                             arg2: libc::c_int, arg3: *const libc::c_char,
                             arg4: libc::c_int) -> ();
    pub fn witness_unlock(arg1: *mut Struct_lock_object, arg2: libc::c_int,
                          arg3: *const libc::c_char, arg4: libc::c_int)
     -> ();
    pub fn witness_save(arg1: *mut Struct_lock_object,
                        arg2: *mut *const libc::c_char,
                        arg3: *mut libc::c_int) -> ();
    pub fn witness_restore(arg1: *mut Struct_lock_object,
                           arg2: *const libc::c_char, arg3: libc::c_int)
     -> ();
    pub fn witness_list_locks(arg1: *mut *mut Struct_lock_list_entry,
                              arg2:
                                  ::core::option::Option<extern "C" fn(arg1:
                                                                          *const libc::c_char, ...)
                                                            -> libc::c_int>)
     -> libc::c_int;
    pub fn witness_warn(arg1: libc::c_int, arg2: *mut Struct_lock_object,
                        arg3: *const libc::c_char, ...) -> libc::c_int;
    pub fn witness_assert(arg1: *const Struct_lock_object,
                          arg2: libc::c_int, arg3: *const libc::c_char,
                          arg4: libc::c_int) -> ();
    pub fn witness_display_spinlock(arg1: *mut Struct_lock_object,
                                    arg2: *mut Struct_thread,
                                    arg3:
                                        ::core::option::Option<extern "C" fn(arg1:
                                                                                *const libc::c_char, ...)
                                                                  ->
                                                                      libc::c_int>)
     -> ();
    pub fn witness_line(arg1: *mut Struct_lock_object) -> libc::c_int;
    pub fn witness_norelease(arg1: *mut Struct_lock_object) -> ();
    pub fn witness_releaseok(arg1: *mut Struct_lock_object) -> ();
    pub fn witness_file(arg1: *mut Struct_lock_object)
     -> *const libc::c_char;
    pub fn witness_thread_exit(arg1: *mut Struct_thread) -> ();
    pub fn read_cpu_time(cp_time: *mut libc::c_long) -> ();
    pub fn cpu_pcpu_init(pcpu: *mut Struct_pcpu, cpuid: libc::c_int,
                         size: size_t) -> ();
    pub fn db_show_mdpcpu(pcpu: *mut Struct_pcpu) -> ();
    pub fn dpcpu_alloc(size: libc::c_int) -> *mut libc::c_void;
    pub fn dpcpu_copy(s: *mut libc::c_void, size: libc::c_int) -> ();
    pub fn dpcpu_free(s: *mut libc::c_void, size: libc::c_int) -> ();
    pub fn dpcpu_init(dpcpu: *mut libc::c_void, cpuid: libc::c_int) -> ();
    pub fn pcpu_destroy(pcpu: *mut Struct_pcpu) -> ();
    pub fn pcpu_find(cpuid: u_int) -> *mut Struct_pcpu;
    pub fn pcpu_init(pcpu: *mut Struct_pcpu, cpuid: libc::c_int,
                     size: size_t) -> ();
    pub fn lockstat_nsecs(arg1: *mut Struct_lock_object) -> uint64_t;
    pub fn _mtx_init(c: *mut uintptr_t, name: *const libc::c_char,
                     _type: *const libc::c_char, opts: libc::c_int) -> ();
    pub fn _mtx_destroy(c: *mut uintptr_t) -> ();
    pub fn mtx_sysinit(arg: *mut libc::c_void) -> ();
    pub fn _mtx_trylock_flags_(c: *mut uintptr_t, opts: libc::c_int,
                               file: *const libc::c_char,
                               line: libc::c_int) -> libc::c_int;
    pub fn mutex_init() -> ();
    pub fn __mtx_lock_sleep(c: *mut uintptr_t, tid: uintptr_t,
                            opts: libc::c_int, file: *const libc::c_char,
                            line: libc::c_int) -> ();
    pub fn __mtx_unlock_sleep(c: *mut uintptr_t, opts: libc::c_int,
                              file: *const libc::c_char,
                              line: libc::c_int) -> ();
    pub fn __mtx_lock_flags(c: *mut uintptr_t, opts: libc::c_int,
                            file: *const libc::c_char, line: libc::c_int)
     -> ();
    pub fn __mtx_unlock_flags(c: *mut uintptr_t, opts: libc::c_int,
                              file: *const libc::c_char,
                              line: libc::c_int) -> ();
    pub fn __mtx_lock_spin_flags(c: *mut uintptr_t, opts: libc::c_int,
                                 file: *const libc::c_char,
                                 line: libc::c_int) -> ();
    pub fn __mtx_unlock_spin_flags(c: *mut uintptr_t, opts: libc::c_int,
                                   file: *const libc::c_char,
                                   line: libc::c_int) -> ();
    pub fn thread_lock_flags_(arg1: *mut Struct_thread, arg2: libc::c_int,
                              arg3: *const libc::c_char,
                              arg4: libc::c_int) -> ();
    pub fn mtx_pool_create(mtx_name: *const libc::c_char,
                           pool_size: libc::c_int, opts: libc::c_int)
     -> *mut Struct_mtx_pool;
    pub fn mtx_pool_destroy(poolp: *mut *mut Struct_mtx_pool) -> ();
    pub fn mtx_pool_find(pool: *mut Struct_mtx_pool, ptr: *mut libc::c_void)
     -> *mut Struct_mtx;
    pub fn mtx_pool_alloc(pool: *mut Struct_mtx_pool) -> *mut Struct_mtx;
    pub fn eventhandler_register(list: *mut Struct_eventhandler_list,
                                 name: *const libc::c_char,
                                 func: *mut libc::c_void,
                                 arg: *mut libc::c_void,
                                 priority: libc::c_int) -> eventhandler_tag;
    pub fn eventhandler_deregister(list: *mut Struct_eventhandler_list,
                                   tag: eventhandler_tag) -> ();
    pub fn eventhandler_find_list(name: *const libc::c_char)
     -> *mut Struct_eventhandler_list;
    pub fn eventhandler_prune_list(list: *mut Struct_eventhandler_list) -> ();
    pub fn clone_setup(cdp: *mut *mut Struct_clonedevs) -> ();
    pub fn clone_cleanup(arg1: *mut *mut Struct_clonedevs) -> ();
    pub fn clone_create(arg1: *mut *mut Struct_clonedevs,
                        arg2: *mut Struct_cdevsw, unit: *mut libc::c_int,
                        dev: *mut *mut Struct_cdev, extra: libc::c_int)
     -> libc::c_int;
    pub fn count_dev(_dev: *mut Struct_cdev) -> libc::c_int;
    pub fn delist_dev(_dev: *mut Struct_cdev) -> ();
    pub fn destroy_dev(_dev: *mut Struct_cdev) -> ();
    pub fn destroy_dev_sched(dev: *mut Struct_cdev) -> libc::c_int;
    pub fn destroy_dev_sched_cb(dev: *mut Struct_cdev,
                                cb:
                                    ::core::option::Option<extern "C" fn(arg1:
                                                                            *mut libc::c_void)
                                                              -> ()>,
                                arg: *mut libc::c_void) -> libc::c_int;
    pub fn destroy_dev_drain(csw: *mut Struct_cdevsw) -> ();
    pub fn drain_dev_clone_events() -> ();
    pub fn dev_refthread(_dev: *mut Struct_cdev, _ref: *mut libc::c_int)
     -> *mut Struct_cdevsw;
    pub fn devvn_refthread(vp: *mut Struct_vnode, devp: *mut *mut Struct_cdev,
                           _ref: *mut libc::c_int) -> *mut Struct_cdevsw;
    pub fn dev_relthread(_dev: *mut Struct_cdev, _ref: libc::c_int) -> ();
    pub fn dev_depends(_pdev: *mut Struct_cdev, _cdev: *mut Struct_cdev)
     -> ();
    pub fn dev_ref(dev: *mut Struct_cdev) -> ();
    pub fn dev_refl(dev: *mut Struct_cdev) -> ();
    pub fn dev_rel(dev: *mut Struct_cdev) -> ();
    pub fn dev_strategy(dev: *mut Struct_cdev, bp: *mut Struct_buf) -> ();
    pub fn dev_strategy_csw(dev: *mut Struct_cdev, csw: *mut Struct_cdevsw,
                            bp: *mut Struct_buf) -> ();
    pub fn make_dev(_devsw: *mut Struct_cdevsw, _unit: libc::c_int,
                    _uid: uid_t, _gid: gid_t, _perms: libc::c_int,
                    _fmt: *const libc::c_char, ...) -> *mut Struct_cdev;
    pub fn make_dev_cred(_devsw: *mut Struct_cdevsw, _unit: libc::c_int,
                         _cr: *mut Struct_ucred, _uid: uid_t, _gid: gid_t,
                         _perms: libc::c_int,
                         _fmt: *const libc::c_char, ...)
     -> *mut Struct_cdev;
    pub fn make_dev_credf(_flags: libc::c_int, _devsw: *mut Struct_cdevsw,
                          _unit: libc::c_int, _cr: *mut Struct_ucred,
                          _uid: uid_t, _gid: gid_t, _mode: libc::c_int,
                          _fmt: *const libc::c_char, ...)
     -> *mut Struct_cdev;
    pub fn make_dev_p(_flags: libc::c_int, _cdev: *mut *mut Struct_cdev,
                      _devsw: *mut Struct_cdevsw, _cr: *mut Struct_ucred,
                      _uid: uid_t, _gid: gid_t, _mode: libc::c_int,
                      _fmt: *const libc::c_char, ...) -> libc::c_int;
    pub fn make_dev_alias(_pdev: *mut Struct_cdev,
                          _fmt: *const libc::c_char, ...)
     -> *mut Struct_cdev;
    pub fn make_dev_alias_p(_flags: libc::c_int,
                            _cdev: *mut *mut Struct_cdev,
                            _pdev: *mut Struct_cdev,
                            _fmt: *const libc::c_char, ...)
     -> libc::c_int;
    pub fn make_dev_physpath_alias(_flags: libc::c_int,
                                   _cdev: *mut *mut Struct_cdev,
                                   _pdev: *mut Struct_cdev,
                                   _old_alias: *mut Struct_cdev,
                                   _physpath: *const libc::c_char)
     -> libc::c_int;
    pub fn dev_lock() -> ();
    pub fn dev_unlock() -> ();
    pub fn setconf() -> ();
    pub fn devfs_get_cdevpriv(datap: *mut *mut libc::c_void)
     -> libc::c_int;
    pub fn devfs_set_cdevpriv(_priv: *mut libc::c_void, dtr: cdevpriv_dtr_t)
     -> libc::c_int;
    pub fn devfs_clear_cdevpriv() -> ();
    pub fn devfs_fpdrop(fp: *mut Struct_file) -> ();
    pub fn devfs_alloc_cdp_inode() -> ino_t;
    pub fn devfs_free_cdp_inode(ino: ino_t) -> ();
    pub fn dev_stdclone(_name: *mut libc::c_char,
                        _namep: *mut *mut libc::c_char,
                        _stem: *const libc::c_char,
                        _unit: *mut libc::c_int) -> libc::c_int;
    pub fn set_dumper(arg1: *mut Struct_dumperinfo,
                      _devname: *const libc::c_char) -> libc::c_int;
    pub fn dump_write(arg1: *mut Struct_dumperinfo, arg2: *mut libc::c_void,
                      arg3: vm_offset_t, arg4: off_t, arg5: size_t)
     -> libc::c_int;
    pub fn dumpsys(arg1: *mut Struct_dumperinfo) -> ();
    pub fn doadump(arg1: boolean_t) -> libc::c_int;
    pub fn cloneuio(uiop: *mut Struct_uio) -> *mut Struct_uio;
    pub fn copyinfrom(src: *const libc::c_void, dst: *mut libc::c_void,
                      len: size_t, seg: libc::c_int) -> libc::c_int;
    pub fn copyiniov(iovp: *const Struct_iovec, iovcnt: u_int,
                     iov: *mut *mut Struct_iovec, error: libc::c_int)
     -> libc::c_int;
    pub fn copyinstrfrom(src: *const libc::c_void, dst: *mut libc::c_void,
                         len: size_t, copied: *mut size_t, seg: libc::c_int)
     -> libc::c_int;
    pub fn copyinuio(iovp: *const Struct_iovec, iovcnt: u_int,
                     uiop: *mut *mut Struct_uio) -> libc::c_int;
    pub fn copyout_map(td: *mut Struct_thread, addr: *mut vm_offset_t,
                       sz: size_t) -> libc::c_int;
    pub fn copyout_unmap(td: *mut Struct_thread, addr: vm_offset_t,
                         sz: size_t) -> libc::c_int;
    pub fn physcopyin(src: *mut libc::c_void, dst: vm_paddr_t, len: size_t)
     -> libc::c_int;
    pub fn physcopyout(src: vm_paddr_t, dst: *mut libc::c_void, len: size_t)
     -> libc::c_int;
    pub fn uiomove(cp: *mut libc::c_void, n: libc::c_int,
                   uio: *mut Struct_uio) -> libc::c_int;
    pub fn uiomove_frombuf(buf: *mut libc::c_void, buflen: libc::c_int,
                           uio: *mut Struct_uio) -> libc::c_int;
    pub fn uiomove_fromphys(ma: *mut *mut Struct_vm_page, offset: vm_offset_t,
                            n: libc::c_int, uio: *mut Struct_uio)
     -> libc::c_int;
    pub fn uiomove_nofault(cp: *mut libc::c_void, n: libc::c_int,
                           uio: *mut Struct_uio) -> libc::c_int;
    pub fn uiomove_object(obj: *mut Struct_vm_object, obj_size: off_t,
                          uio: *mut Struct_uio) -> libc::c_int;
    pub fn contigfree(addr: *mut libc::c_void, size: libc::c_ulong,
                      _type: *mut Struct_malloc_type) -> ();
    pub fn contigmalloc(size: libc::c_ulong, _type: *mut Struct_malloc_type,
                        flags: libc::c_int, low: vm_paddr_t,
                        high: vm_paddr_t, alignment: libc::c_ulong,
                        boundary: vm_paddr_t) -> *mut libc::c_void;
    pub fn free(addr: *mut libc::c_void, _type: *mut Struct_malloc_type)
     -> ();
    pub fn malloc(size: libc::c_ulong, _type: *mut Struct_malloc_type,
                  flags: libc::c_int) -> *mut libc::c_void;
    pub fn malloc_init(arg1: *mut libc::c_void) -> ();
    pub fn malloc_last_fail() -> libc::c_int;
    pub fn malloc_type_allocated(_type: *mut Struct_malloc_type,
                                 size: libc::c_ulong) -> ();
    pub fn malloc_type_freed(_type: *mut Struct_malloc_type,
                             size: libc::c_ulong) -> ();
    pub fn malloc_type_list(arg1:
                                *mut ::core::option::Option<extern "C" fn()
                                                               -> ()>,
                            arg2: *mut libc::c_void) -> ();
    pub fn malloc_uninit(arg1: *mut libc::c_void) -> ();
    pub fn realloc(addr: *mut libc::c_void, size: libc::c_ulong,
                   _type: *mut Struct_malloc_type, flags: libc::c_int)
     -> *mut libc::c_void;
    pub fn reallocf(addr: *mut libc::c_void, size: libc::c_ulong,
                    _type: *mut Struct_malloc_type, flags: libc::c_int)
     -> *mut libc::c_void;
    pub fn malloc_desc2type(desc: *const libc::c_char)
     -> *mut Struct_malloc_type;
}
